package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.RemoteException;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 25.1.2016..
 */
public class Model {

    // Default Content Authority is currently hardcoded in the Model, but it can be overrriden...
    protected String mAuthority = FeedContract.CONTENT_AUTHORITY;

    protected Uri mUri;

    protected List<ContentProviderOperation> batch;

    public long _id;

    public int synced;

    protected ContentResolver mContentResolver;

    public Model(){
        batch = new ArrayList<>();
    }

    public boolean isPersisted(){
        return _id != 0;
    }

    public List<ContentProviderOperation> save(Context context){
        if(mContentResolver == null){
            mContentResolver = context.getContentResolver();
        }
        return batch;
    }

    public void delete(){
    }

    public void commit(List<ContentProviderOperation> batch) throws RemoteException, OperationApplicationException {
        mContentResolver.applyBatch(mAuthority, (ArrayList<ContentProviderOperation>) batch);
    }
}
