package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.Context;

import com.google.gson.annotations.Expose;

import java.util.List;
import java.util.UUID;

/**
 * Created by Antonio on 27.1.2016..
 */
public abstract class ModelWithUuid extends Model {

    @Expose
    public String uuid;

    public ModelWithUuid(){
        uuid = UUID.randomUUID().toString();
    }

    public List<ContentProviderOperation> save(Context context){
        super.save(context);
        return batch;
    }
}
