package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.ModelRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 26.1.2016..
 */
public class GeoNote extends Note {

    //todo initialize
    private Geo geo;

    public GeoNote(){
    }

    /**
     * Use this constructor for creating a new GeoNote.
     *
     * @param title
     * @param text
     * @param date_alarm
     * @param priority
     * @param color
     * @param labels_id
     */
    public GeoNote(String title, String text, long date_alarm, int priority, String color, String labels_id){
        this.title = title;
        this.text = text;
        this.date_alarm = date_alarm;
        this.priority = priority;
        this.color = color;
        this.labels_id = labels_id;
        this.type = FeedContract.Note.TYPE_GEO;
    }

    public GeoNote(long id, String uuid, String title, long date_created, long date_updated,
                   long date_alarm, String text, int priority, String color, int status, String labels_id, String geos_id) {
        this(uuid, title, date_created, date_updated, date_alarm, text, priority, color, status, labels_id, geos_id);
        this._id = id;
    }

    public GeoNote(String uuid, String title, long date_created, long date_updated, long date_alarm, String text, int priority, String color, int status, String labels_id, String geos_id) {
        super(uuid, title, date_created, date_updated, date_alarm, text, priority, color, status, labels_id, geos_id);
        this.type = FeedContract.Note.TYPE_GEO;
    }

    public Geo getGeo(Context context){
        // todo implement a way to fetch geo information, or provide getters or something
        if (geo == null) {
            Cursor c = null;
            String[] args = {"" + this.geos_id};
            c = ModelRepository.getInstance(context).findModels(Geo.class.getName(),
                    FeedContract.Geo.COLUMN_NAME_UUID + "=?", args, null);
            if (c != null) {
                while (c.moveToNext()) {
                    geo = Geo.fromCursor(c);
                }
            }
            if (c != null)
                c.close();
        }
        return geo;
    }

    public static GeoNote fromCursor(Cursor cursor){
        /*return (GeoNote)Note.fromCursor(cursor);*/
        return new GeoNote(
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UUID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TITLE)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_CREATED)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UPDATED)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ALARM)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TEXT)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_PRIORITY)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_COLOR)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_STATUS)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_LABELS_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_GEOS_ID)));
    }

    public void addGeo(Geo geo) {
        this.geos_id = geo.uuid;
        this.geo = geo;
    }

    @Override
    public List<ContentProviderOperation> save(Context context) {
        super.save(context);
        batch.addAll(this.geo.save(context));
        return batch;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
    }

    protected GeoNote(Parcel in) {
        super(in);
    }

    public static final Creator<GeoNote> CREATOR = new Creator<GeoNote>() {
        public GeoNote createFromParcel(Parcel source) {
            return new GeoNote(source);
        }

        public GeoNote[] newArray(int size) {
            return new GeoNote[size];
        }
    };
}
