package com.notes.map.pinit.basicsyncadapter.sync;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.collections.UsersNotesCollection;
import com.notes.map.pinit.basicsyncadapter.models.UserNote;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.common.utils.Tuple;
import com.notes.map.pinit.services.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Antonio on 24.1.2016..
 */
public class UserNotesSyncManager extends AbstractSyncManager {

    /**
     * Projection used when querying content provider. Returns all known fields.
     */
    public static final String[] PROJECTION = new String[]{
            FeedContract.UserNote._ID,
            FeedContract.UserNote.COLUMN_NAME_NOTES_UUID,
            FeedContract.UserNote.COLUMN_NAME_API_KEY,
            FeedContract.UserNote.COLUMN_NAME_CAN_EDIT,
            FeedContract.UserNote.COLUMN_NAME_CAN_DELETE,
            FeedContract.UserNote.COLUMN_NAME_OWNER,
            FeedContract.UserNote.COLUMN_NAME_UPDATED,
            FeedContract.UserNote.COLUMN_NAME_DELETED,
            FeedContract.UserNote.COLUMN_NAME_SYNCED
    };

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
    public static final int COLUMN_NOTES_UUID = 1;
    public static final int COLUMN_API_KEY = 2;
    /*public static final int COLUMN_USER_EMAIL = 2;*/
    public static final int COLUMN_CAN_EDIT = 3;
    public static final int COLUMN_CAN_DELETE = 4;
    public static final int COLUMN_OWNER = 5;
    public static final int COLUMN_UPDATED = 6;
    public static final int COLUMN_DELETED = 7;
    public static final int COLUMN_SYNCED = 8;


    private UsersNotesCollection newUsersNotes;
    private UsersNotesCollection updatedUsersNotes;
    private ArrayList<UserNote> deletedUsersNotes;
    private Response<UsersNotesCollection> response;

    public UserNotesSyncManager(final Context context, Retrofit client, PinitApiInterface service, List<ContentProviderOperation> batch) {
        super(context, client, service, batch);
        // Build hash table of incoming entries
        //build a collection of new local entries
        newUsersNotes = new UsersNotesCollection();
        updatedUsersNotes = new UsersNotesCollection();
        deletedUsersNotes = new ArrayList<UserNote>();
    }

    public void syncNewEntries(final SyncResult syncResult) throws IOException {
        //todo read last date of sync
        Call<UsersNotesCollection> call = service.getUserNotes(
                SessionManager.getInstance(mContext).getSessionApiKey(),
                SessionManager.getInstance(mContext).getSyncTimestamp(FeedContract.UserNote.TABLE_NAME));

        response = call.execute();

        if (!response.isSuccess()){
            //todo think of a proper way to handle all status codes
            throw new IOException("Network request error code" + response.code());
        }

        if (response.body() == null)
            return;

        HashMap<Tuple, UserNote> entryMap = new HashMap<>();
        for (UserNote e : response.body().users_notes) {
            String[] contents = {e.users_api_key, e.notes_uuid};
            entryMap.put(new Tuple(contents), e);
        }

        // Get list of all items
        Log.i(this.getClass().toString(), "Fetching local entries for merge");
        Uri uri = FeedContract.UserNote.CONTENT_URI; // Get all entries
        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        if ( c != null ) {
            Log.i(this.getClass().toString(), "Found " + c.getCount() + " local entries. Computing merge solution...");

            // Find stale data
            String api_key;
            String notes_uuid;
            int can_edit;
            int can_delete;
            int owner;
            long updated;
            int deleted;
            int synced;

            while (c.moveToNext()) {
                syncResult.stats.numEntries++;
                // fetch all data
                api_key = c.getString(COLUMN_API_KEY);
                notes_uuid = c.getString(COLUMN_NOTES_UUID);
                can_edit = c.getInt(COLUMN_CAN_EDIT);
                can_delete = c.getInt(COLUMN_CAN_DELETE);
                owner = c.getInt(COLUMN_OWNER);
                updated = c.getLong(COLUMN_UPDATED);
                can_delete = c.getInt(COLUMN_DELETED);
                synced = c.getInt(COLUMN_SYNCED);
                deleted = c.getInt(COLUMN_DELETED);
                String[] content = {api_key, notes_uuid};
                Tuple id_tuple = new Tuple(content);
                UserNote match = entryMap.get(id_tuple);
                if (match != null) {
                    // Entry exists. Update it and remove from Map to prevent duplicate inserts.
                    entryMap.remove(id_tuple);

                    Uri existingUri = FeedContract.Note.CONTENT_URI.buildUpon().appendEncodedPath(api_key).appendPath(notes_uuid).build();
                    //ignore an already stale entry or an already deleted entry
                    if (match.date_updated > updated && match.deleted != FeedContract.SyncableItem.DELETED && synced != FeedContract.SyncableItem.SYNC_STATUS_DELETED) {
                        // Update existing record
                        Log.i(this.getClass().toString(), "Scheduling update: " + existingUri);
                        batch.add(ContentProviderOperation.newUpdate(existingUri)
                                .withValue(FeedContract.UserNote.COLUMN_NAME_CAN_EDIT, match.can_edit)
                                .withValue(FeedContract.UserNote.COLUMN_NAME_CAN_DELETE, match.can_delete)
                                .withValue(FeedContract.UserNote.COLUMN_NAME_OWNER, match.owner)
                                .withValue(FeedContract.UserNote.COLUMN_NAME_UPDATED, match.date_updated)
                                .withValue(FeedContract.UserNote.COLUMN_NAME_SYNCED, FeedContract.UserNote.SYNC_STATUS_SYNCED)
                                .build());
                        syncResult.stats.numUpdates++;
                    } else if (match.deleted == FeedContract.SyncableItem.DELETED){
                        // server deleted it, delete locally
                        batch.add(ContentProviderOperation.newDelete(existingUri).build());
                        syncResult.stats.numDeletes++;
                    } else if (updated > match.date_updated && deleted != FeedContract.SyncableItem.DELETED){
                        //local is newer, update server
                        updatedUsersNotes.add(new UserNote(notes_uuid, api_key, can_edit, can_delete, owner, updated, deleted));
                    } else if (deleted == FeedContract.SyncableItem.DELETED){
                        // deleted locally, server is not up to date
                        deletedUsersNotes.add(new UserNote(notes_uuid, api_key, can_edit, can_delete, owner, updated, deleted));
                    }

                    // non matching, but "dirty" with CREATED flag set, add to the newCollection
                } else if (synced == FeedContract.UserNote.SYNC_STATUS_CREATED) {
                    newUsersNotes.add(new UserNote(notes_uuid, api_key, can_edit, can_delete, owner, updated, deleted));
                    // non matching, but "dirty" with DIRTY flag set, add to the updatedCollection
                } else if (synced == FeedContract.UserNote.SYNC_STATUS_UPDATED) {
                    updatedUsersNotes.add(new UserNote(notes_uuid, api_key, can_edit, can_delete, owner, updated, deleted));
                    // non matching, but "dirty" with DELETED flag set, add to the deletedCollection
                } else if (synced == FeedContract.UserNote.SYNC_STATUS_DELETED) {
                    deletedUsersNotes.add(new UserNote(notes_uuid, api_key, can_edit, can_delete, owner, updated, deleted));
                }
            }
            c.close();
        }
        // Add fetched non existing entries to database
        for (UserNote note : entryMap.values()) {
            Log.i(this.getClass().toString(), "Scheduling insert: entry_id=<" + note.notes_uuid + "," + note.users_api_key + ">");
            batch.add(ContentProviderOperation.newInsert(FeedContract.UserNote.CONTENT_URI)
                    .withValue(FeedContract.UserNote.COLUMN_NAME_API_KEY, note.users_api_key)
                    .withValue(FeedContract.UserNote.COLUMN_NAME_NOTES_UUID, note.notes_uuid)
                    .withValue(FeedContract.UserNote.COLUMN_NAME_CAN_EDIT, note.can_edit)
                    .withValue(FeedContract.UserNote.COLUMN_NAME_CAN_DELETE, note.can_delete)
                    .withValue(FeedContract.UserNote.COLUMN_NAME_OWNER, note.owner)
                    .withValue(FeedContract.UserNote.COLUMN_NAME_UPDATED, note.date_updated)
                    .withValue(FeedContract.UserNote.COLUMN_NAME_SYNCED, FeedContract.UserNote.SYNC_STATUS_SYNCED)
                    .build());
            syncResult.stats.numInserts++;
        }

    }

    public void syncCreatedEntries(final SyncResult syncResult) throws IOException {
        // Let's POST all the new "dirty" entries (that have status = CREATED)
        if (newUsersNotes.size() > 0) {
            Call<UsersNotesCollection> createdCall = service.createUsersNotes(SessionManager.getInstance(mContext).getSessionApiKey(), newUsersNotes);
            response = createdCall.execute();
            if (response.isSuccess()) {
                for (UserNote note : response.body().users_notes) {
                    Uri existingUri = FeedContract.UserNote.CONTENT_URI.buildUpon()
                            .appendEncodedPath(note.users_api_key).appendPath(note.notes_uuid).build();
                    // we put tne note status to normal and mark it synced
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.UserNote.COLUMN_NAME_SYNCED, FeedContract.UserNote.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
        }
    }

    public void syncUpdatedEntries(final SyncResult syncResult) throws IOException {
        // Let's PUT all the updated entries
        // todo think of a correct way to solve (very unlikely) conflicts
        // todo try refactoring into a generic method using metaprogramming, to stay DRY
        if (updatedUsersNotes.size() > 0) {
            Call<UsersNotesCollection> updatedCall = service.updateUsersNotes(SessionManager.getInstance(mContext).getSessionApiKey(), updatedUsersNotes);
            response = updatedCall.execute();
            if(response.isSuccess()) {
                for (UserNote note : response.body().users_notes) {
                    Uri existingUri = FeedContract.UserNote.CONTENT_URI.buildUpon()
                            .appendEncodedPath(note.users_api_key).appendPath(note.notes_uuid).build();
                    // we put tne note status to normal and mark it synced
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.UserNote.COLUMN_NAME_SYNCED, FeedContract.UserNote.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
        }
    }

    public void syncDeletedEntries(final SyncResult syncResult) throws IOException {

        // Let's DELETE all the deleted entries
        if (deletedUsersNotes.size() > 0) {
            for (UserNote userNote : deletedUsersNotes) {
                Call<UserNote> deletedCall = service.deleteUserNote(
                        SessionManager.getInstance(mContext).getSessionApiKey(),
                        userNote.users_api_key, userNote.notes_uuid);
                Response<UserNote> response = deletedCall.execute();
                if (response.isSuccess()) {
                    Uri existingUri = FeedContract.Note.CONTENT_URI.buildUpon()
                            .appendEncodedPath(userNote.users_api_key).appendPath(userNote.notes_uuid).build();
                    batch.add(ContentProviderOperation.newDelete(existingUri)
                            .build());
                }
            }
        }

    }

    @Override
    protected Uri buildUri(String id) {
        throw new UnsupportedOperationException("Not supported, use buildUri(userId, noteId) instead");
    }
}
