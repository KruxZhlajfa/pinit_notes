package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.Cursor;
import android.os.Parcel;

import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.ModelRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 26.1.2016..
 */
public class TodoNote extends Note {

    private List<Item> items;

    public TodoNote(){
    }

    public TodoNote(String title, long date_alarm, int priority, String color, int status, String labels_id) {
        super(title, date_alarm, null, priority, color, status, labels_id);
        this.type = FeedContract.Note.TYPE_TODO;
    }

    public TodoNote(long id, String uuid, String title, long date_created, long date_updated, long date_alarm, String text, int priority, String color, int status, String labels_id, String geos_id) {
        super(id, uuid, title, date_created, date_updated, date_alarm, text, priority, color, status, labels_id, geos_id, FeedContract.Note.TYPE_TODO);
    }

    public List<Item> getTodos(Context context) {
        if (items == null) {
            items = new ArrayList<>();
            Cursor c = null;
            String[] args = {"" + uuid};
            c = ModelRepository.getInstance(context).findModels(Item.class.getName(),
                    FeedContract.Item.COLUMN_NAME_NOTES_ID + "=? AND "
                            + FeedContract.Item.COLUMN_NAME_DELETED + "== 0", args, null);
            if (c != null) {
                while (c.moveToNext()) {
                    items.add(Item.fromCursor(c));
                }
            }
            if (c != null)
                c.close();
        }
        return items;
    }

    public Item getTodos(int i){
        if(i < items.size())
            return items.get(i);
        return null;
    }

    @Override
    public void delete() {
        super.delete();
        for(Item item : items)
            item.delete();
    }

    public void removeTodo(Item item){
        // not yet saved, remove from list only
        if (!item.isPersisted())
            items.remove(item);
        else
            item.delete();
    }

    public void addTodo(Item item){
        if (items == null){
            items = new ArrayList<>();
        }
        // add the foreign key
        item.notes_id = uuid;
        items.add(item);
    }

    public static TodoNote fromCursor(Cursor cursor){
        return new TodoNote(
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UUID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TITLE)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_CREATED)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UPDATED)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ALARM)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TEXT)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_PRIORITY)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_COLOR)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_STATUS)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_LABELS_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_GEOS_ID)));
    }

    @Override
    public List<ContentProviderOperation> save(Context context) {
        batch = super.save(context);
        for(Item i : getTodos(context)){
            batch.addAll(i.save(context));
        }
        return batch;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        if (this.items != null)
            dest.writeList(this.items);
    }

    protected TodoNote(Parcel in) {
        super(in);
        this.items = new ArrayList<Item>();
        in.readList(this.items, List.class.getClassLoader());
        if (this.items.size() == 0)
            items = null;
    }

    public static final Creator<TodoNote> CREATOR = new Creator<TodoNote>() {
        public TodoNote createFromParcel(Parcel source) {
            return new TodoNote(source);
        }

        public TodoNote[] newArray(int size) {
            return new TodoNote[size];
        }
    };
}
