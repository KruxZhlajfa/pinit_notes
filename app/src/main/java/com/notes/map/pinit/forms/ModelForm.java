package com.notes.map.pinit.forms;

import com.notes.map.pinit.basicsyncadapter.models.Model;

/**
 * Created by Antonio on 27.1.2016..
 */
public abstract class ModelForm {

    /**
     * Validate the data in this form.
     * @return
     */
    public abstract boolean isValid();

    /**
     * Return the Model that is used in this form, with the new data bound to it.
     * @param <T>
     * @return
     */
    public abstract <T extends Model> Model getModel();

}
