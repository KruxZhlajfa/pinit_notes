package com.notes.map.pinit.basicsyncadapter.collections;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.models.Model;

import java.util.List;
import java.util.Map;

/**
 * Created by Antonio on 23.1.2016..
 *
 *
 * Top level wrapper objects for API responses that return collections of objects.
 *
 */
public abstract class AbstractCollection {

    // todo add other parameters if required

    @Expose(serialize = false)
    public String sync_timestamp;

    @Expose(serialize = false)
    public Map<String , List<String>> errors;

    public abstract  <T extends Model> T get(String id);
}
