package com.notes.map.pinit.basicsyncadapter;

import com.notes.map.pinit.basicsyncadapter.collections.GeosCollection;
import com.notes.map.pinit.basicsyncadapter.collections.ItemsCollection;
import com.notes.map.pinit.basicsyncadapter.collections.LabelsCollection;
import com.notes.map.pinit.basicsyncadapter.collections.NotesCollection;
import com.notes.map.pinit.basicsyncadapter.collections.UsersCollection;
import com.notes.map.pinit.basicsyncadapter.collections.UsersNotesCollection;
import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.models.User;
import com.notes.map.pinit.basicsyncadapter.models.UserNote;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


/**
 * Created by Antonio on 22.1.2016..
 */
public interface PinitApiInterface {

    /** Notes **/

    @GET("/api/notes")
    Call<NotesCollection> getNotes(@Query("api_key") String api_key, @Query("timestamp") long timestamp);

    @POST("/api/notes")
    Call<NotesCollection> createNotes(@Query("api_key") String api_key, @Body NotesCollection notes);

    @PUT("/api/notes")
    Call<NotesCollection> updateNotes(@Query("api_key") String api_key, @Body NotesCollection notes);

    @DELETE("/api/note")
    Call<Note> deleteNote(@Query("api_key") String api_key, @Query("uuid") String uuid);

    /** Labels **/

    @GET("/api/labels")
    Call<LabelsCollection> getLabels(@Query("api_key") String api_key, @Query("timestamp") long timestamp);

    @POST("/api/labels")
    Call<LabelsCollection> createLabels(@Query("api_key") String api_key, @Body LabelsCollection labels);

    @PUT("/api/labels")
    Call<LabelsCollection> updateLabels(@Query("api_key") String api_key, @Body LabelsCollection labels);

    @DELETE("/api/label")
    Call<Label> deleteLabels(@Query("api_key") String api_key, @Query("uuid") String uuid);

    /** Items **/

    @GET("/api/items")
    Call<ItemsCollection> getItems(@Query("api_key") String api_key, @Query("timestamp") long timestamp);

    @POST("/api/items")
    Call<ItemsCollection> createItems(@Query("api_key") String api_key, @Body ItemsCollection items);

    @PUT("/api/items")
    Call<ItemsCollection> updateItems(@Query("api_key") String api_key, @Body ItemsCollection items);

    @DELETE("/api/item")
    Call<Item> deleteItem(@Query("api_key") String api_key, @Query("uuid") String uuid);

    /** Geos **/

    @GET("/api/geos")
    Call<GeosCollection> getGeos(@Query("api_key") String api_key, @Query("timestamp") long timestamp);

    @POST("/api/geos")
    Call<GeosCollection> createGeos(@Query("api_key") String api_key, @Body GeosCollection geos);

    @PUT("/api/geos")
    Call<GeosCollection> updateGeos(@Query("api_key") String api_key, @Body GeosCollection geos);

    @DELETE("/api/geo")
    Call<Geo> deleteGeo(@Query("api_key") String api_key, @Query("uuid") String uuid);

    /** Users **/

    @GET("/api/users")
    Call<UsersCollection> getUsers(@Query("user_email") String api_key, @Query("timestamp") long timestamp);

    @POST("/api/users")
    Call<User> createUser(@Body User user);

    @PUT("/api/user")
    Call<User> updateUser(@Query("email") String email, @Query("api_key") String api_key, @Body User user);

    @DELETE("/api/user")
    Call<User> deleteUser(@Query("email") String email, @Query("api_key") String api_key);


    /** UserNotes **/

    @GET("/api/users/notes")
    Call<UsersNotesCollection> getUserNotes(@Query("api_key") String api_key, @Query("timestamp") long timestamp);

    @POST("/api/users/notes")
    Call<UsersNotesCollection> createUsersNotes(@Query("api_key") String api_key, @Body UsersNotesCollection usersNotes);

    @PUT("/api/users/notes")
    Call<UsersNotesCollection> updateUsersNotes(@Query("api_key") String api_key, @Body UsersNotesCollection usersNotes);

    @DELETE("/api/user/note")
    Call<UserNote> deleteUserNote(@Query("users_api_key") String users_api_key, @Query("api_key") String api_key, @Query("notes_uuid") String notes_uuid);

    //todo add separate routes for todos?

    //todo add archived items routes

    /** Login **/
    @POST("/api/login")
    Call<User> loginUser(@Body User user);
}
