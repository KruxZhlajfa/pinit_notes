package com.notes.map.pinit;

import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.models.GeoNote;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.models.TodoNote;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.forms.ItemForm;
import com.notes.map.pinit.services.ModelRepository;
import com.pes.androidmaterialcolorpickerdialog.ColorPicker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

/**
 * Created by iamkr on 23.1.2016..
 */
public class DetailedActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {


    public final static String ID = "ID";

    public Note mNote;
    public GeoNote mGeoNote;
    private Toolbar mToolbar;
    //view controls
    @InjectView(R.id.button_toolbar ) ImageButton mButtonDelete;
    @InjectView(R.id.button_color_picker) ImageButton mButtonColor;
    @InjectView(R.id.button_add_todo) @Optional ImageButton mAddButton;
    @InjectView(R.id.detailed_layout) LinearLayout mLinearLayout;

    private List<ItemForm> mTodos = new ArrayList<>();

    private ModelRepository mRepository;

    private LinearLayout mParentLayout;

    private EditText mEditTextTitle;
    private EditText mEditTextDesc;
    private EditText mEditTextAddress;

    private ColorPicker cp;
    private Button mOkColorButton;
    private int mIntColor;
    private String mHexColor;

    private Label mSelectedLabel;

    private Cursor labelCursor;

    @InjectView(R.id.spinner_label) Spinner mSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);

        mToolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNote = getIntent().getExtras().getParcelable(ID);
        Log.d("DEBUG", "Detailed view got note type:" + mNote.getType());
        ViewStub stub = (ViewStub) findViewById(R.id.stub_note);
        if (mNote.getType() == FeedContract.Note.TYPE_NORMAL) {
            stub.setLayoutResource(R.layout.notes);
            stub.inflate();
        } else if (mNote.getType() == FeedContract.Note.TYPE_TODO) {
            stub.setLayoutResource(R.layout.todos);
            stub.inflate();
        } else if (mNote.getType() == FeedContract.Note.TYPE_GEO) {
            stub.setLayoutResource(R.layout.geo_note);
            stub.inflate();
        }

        mEditTextTitle = (EditText) findViewById(R.id.notes_title);
        mEditTextTitle.setText(mNote.title);
        if (mNote.getType() == FeedContract.Note.TYPE_NORMAL) {
            mEditTextDesc = (EditText) findViewById(R.id.notes_desc);
            mEditTextDesc.setText(mNote.text);
        } else if (mNote.getType() == FeedContract.Note.TYPE_TODO) {
            mParentLayout = (LinearLayout) findViewById(R.id.todo_root_layout);
            TodoNote note = (TodoNote) mNote;
            // get the notes and create forms
            for (Item item : note.getTodos(getApplicationContext())){
                mTodos.add(new ItemForm(this, mParentLayout, item));
            }
        } else if (mNote.getType() == FeedContract.Note.TYPE_GEO) {
            mGeoNote = (GeoNote) mNote;
            Geo mGeo = mGeoNote.getGeo(getApplicationContext());
            mEditTextDesc = (EditText) findViewById(R.id.geo_desc);
            mEditTextAddress = (EditText) findViewById(R.id.geo_address);
            mEditTextDesc.setText(mGeoNote.text);
            mEditTextAddress.setText(mGeo.address);
        }
        int color = (int)Long.parseLong(mNote.color.substring(1), 16);
        int r = (color >> 16) & 0xFF;
        int g = (color >> 8) & 0xFF;
        int b = (color >> 0) & 0xFF;
        cp = new ColorPicker(DetailedActivity.this, r, g, b);
        mHexColor = mNote.color;

        ButterKnife.inject(this);
        if (mAddButton != null)
            mAddButton.setOnClickListener(this);
        mButtonDelete.setImageResource(R.drawable.ic_delete);
        mButtonDelete.setOnClickListener(this);
        mButtonColor.setOnClickListener(this);

        mLinearLayout.setBackgroundColor(Color.parseColor(mNote.color));

        mRepository = ModelRepository.getInstance(getApplicationContext());

        //todo use CursorLoader to manage the lifecycle of this Cursor, as SimpleCursorAdapter is deprecated
        labelCursor = ModelRepository.getInstance(getApplicationContext()).findLabels();
        SimpleCursorAdapter labelAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_spinner_item,
                labelCursor,
                new String[]{FeedContract.Label.COLUMN_NAME_NAME},
                new int[]{android.R.id.text1});
        labelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(labelAdapter);
        mSpinner.setOnItemSelectedListener(this);
        for (int i = 0; i < mSpinner.getCount(); i++){
            Cursor c = (Cursor) mSpinner.getItemAtPosition(i);
            if(c.getString(c.getColumnIndex(FeedContract.Note.COLUMN_NAME_UUID)).equals(mNote.labels_id)){
                mSpinner.setSelection(i);
            }
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        labelCursor.close();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // save the Note and any related items
                // todo refactor into a NoteForm class to hold every related Model and it's Widgets
                // and to eliminate the if checks
                mNote.title = mEditTextTitle.getText().toString();
                mNote.addLabel(mSelectedLabel);
                mNote.color = mHexColor;
                if (mNote.getType() == FeedContract.Note.TYPE_NORMAL) {
                    mNote.text = mEditTextDesc.getText().toString();
                } else if (mNote.getType() == FeedContract.Note.TYPE_TODO){
                    for (ItemForm itemForm : mTodos) {
                        // automatically delete all empty TodoNote Items
                        if (!itemForm.isValid())
                            ((TodoNote) mNote).removeTodo(itemForm.getModel());
                    }
                } else if (mNote.getType() == FeedContract.Note.TYPE_GEO){
                    mNote.text = mEditTextDesc.getText().toString();
                    GeoNote mGeoNote = (GeoNote) mNote;
                    double mLatitude, mLongitude;
                    Geocoder geocoder = new Geocoder(getApplicationContext());
                    List<Address> address;
                    //todo think of a way to move this to a non UI thread and show a progess dialog
                    try {
                        address = geocoder.getFromLocationName(mEditTextAddress.getText().toString(), 1);
                        if (address == null || address.size() == 0) {
                            mEditTextAddress.setError("Invalid adress! No results found!");
                            return false;
                        }
                        Address location = address.get(0);
                        mLatitude = location.getLatitude();
                        mLongitude = location.getLongitude();
                    } catch (IOException e) {
                        Toast.makeText(DetailedActivity.this, "Error getting coordinates! Check your network connection!", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                        return false;
                    }
                    Geo mGeo = mGeoNote.getGeo(getApplicationContext());
                    mGeo.latitude = mLatitude;
                    mGeo.longitude = mLongitude;
                    mGeo.address = mEditTextAddress.getText().toString();
                }
                try {
                    mNote.commit(mNote.save(getBaseContext()));
                } catch (RemoteException | OperationApplicationException e) {
                    Log.e(getClass().getSimpleName(), e.getMessage());
                    Toast.makeText(getBaseContext(), "Failed to save Note!", Toast.LENGTH_SHORT).show();
                }
                MapTab.addMarkersFromGeoNotes(getApplicationContext());
                finish();
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_toolbar:
                try {
                    switch(mNote.getType()) {
                        case (FeedContract.Note.TYPE_NORMAL):
                            mNote.delete();
                            break;
                        case (FeedContract.Note.TYPE_TODO):
                            ((TodoNote)mNote).delete();
                            break;
                        case (FeedContract.Note.TYPE_GEO):
                            ((GeoNote)mNote).delete();
                            break;
                    }
                    mNote.commit(mNote.save(getBaseContext()));
                    finish();
                } catch (RemoteException | OperationApplicationException e) {
                    Toast.makeText(getBaseContext(), "Failed to delete Note!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                break;
            case R.id.button_color_picker:
                cp.show();
                mOkColorButton = (Button) cp.findViewById(R.id.okColorButton);
                mOkColorButton.setOnClickListener(this);
                break;
            case R.id.button_add_todo:
                Item item = new Item();
                mTodos.add(new ItemForm(this, mParentLayout, item));
                ((TodoNote) mNote).addTodo(item);
                break;
            case R.id.okColorButton:
                mIntColor = cp.getColor();
                mHexColor = String.format("#%06X", (0xFFFFFF & mIntColor));
                mLinearLayout.setBackgroundColor(mIntColor);
                cp.dismiss();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Cursor c = (Cursor) adapterView.getAdapter().getItem(i);
        mSelectedLabel = Label.fromCursor(c);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        mSelectedLabel = null;
    }
}
