package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 */
public class User extends Model {

    @Expose
    public String api_key;
    @Expose
    public String email;
    @Expose
    public String name;
    @Expose
    public String surname;
    @Expose
    public String password = null;
    @Expose
    public String picture;
    @Expose
    public int deleted;
    @Expose
    public String username;
    @Expose
    public long date_updated;

    public User(String email, String password){
        this.email = email;
        this.password = password;
    }

    public User(String email, String name, String surname) {
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.picture = null;
        this.deleted = 0;
    }

    public User(String email, String name, String surname, String password){
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.picture = null;
        this.deleted = 0;
        this.password = password;
    }

    public User(String email, String name, String surname, int deleted, String username) {
        this(email, name, surname);
        this.username = username;
        this.deleted = deleted;
    }

    public User(String email, String name, String surname, String picture, int deleted, String username, long date_updated) {
        this(email, name, surname, deleted, username);
        this.picture = picture;
        this.date_updated = date_updated;
    }

    public User(String api_key, String email, String name, String surname, String picture, int deleted, String username) {
        this(email, name, surname, deleted, username);
        this.api_key = api_key;
        this.picture = picture;
    }

    public User(String api_key, String email, String name, String surname, String picture, int deleted, String username, long date_updated) {
        this(api_key, email, name, surname, picture, deleted, username);
        this.date_updated = date_updated;
    }


    @Override
    public List<ContentProviderOperation> save(Context context) {
        super.save(context);
        this.date_updated = System.currentTimeMillis();
        ContentValues values = new ContentValues();
        values.put(FeedContract.User.COLUMN_NAME_EMAIL, this.email);
        values.put(FeedContract.User.COLUMN_NAME_NAME, this.name);
        values.put(FeedContract.User.COLUMN_NAME_SURNAME, this.surname);
        values.put(FeedContract.User.COLUMN_NAME_PICTURE, this.picture.toString()); //todo see if we can store blob like this
        values.put(FeedContract.User.COLUMN_NAME_DELETED, this.deleted);
        values.put(FeedContract.User.COLUMN_NAME_USERNAME, this.username);
        values.put(FeedContract.User.COLUMN_NAME_UPDATED, this.date_updated);
        if (this._id != 0){
            //update
            values.put(FeedContract.User.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_UPDATED);
            String[] selection = {username};
            mContentResolver.update(FeedContract.User.CONTENT_URI, values, FeedContract.User.COLUMN_NAME_USERNAME + "=?", selection);
        } else {
            //insert new
            values.put(FeedContract.User.COLUMN_NAME_API_KEY, this.api_key);
            values.put(FeedContract.User.COLUMN_NAME_USERNAME, this.username);
            values.put(FeedContract.User.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_CREATED);
            Uri uri = mContentResolver.insert(FeedContract.User.CONTENT_URI, values);
            this._id = ContentUris.parseId(uri);
        }
        return batch;
    }

    @Override
    public void delete() {
        this.deleted = FeedContract.SyncableItem.DELETED;
    }
}
