package com.notes.map.pinit.basicsyncadapter.sync;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.collections.UsersCollection;
import com.notes.map.pinit.basicsyncadapter.models.User;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Antonio on 24.1.2016..
 */
public class UsersSyncManager extends AbstractSyncManager {


    public static final String[] PROJECTION = new String[]{
            FeedContract.User._ID,
            FeedContract.User.COLUMN_NAME_API_KEY,
            FeedContract.User.COLUMN_NAME_EMAIL,
            FeedContract.User.COLUMN_NAME_NAME,
            FeedContract.User.COLUMN_NAME_SURNAME,
            FeedContract.User.COLUMN_NAME_PASSWORD,
            FeedContract.User.COLUMN_NAME_PICTURE,
            FeedContract.User.COLUMN_NAME_DELETED,
            FeedContract.User.COLUMN_NAME_USERNAME,
            FeedContract.User.COLUMN_NAME_SYNCED
    };

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
    public static final int COLUMN_API_KEY = 1;
    public static final int COLUMN_EMAIL = 2;
    public static final int COLUMN_NAME = 3;
    public static final int COLUMN_SURNAME = 4;
    public static final int COLUMN_PASSWORD = 5;
    public static final int COLUMN_PICTURE = 6;
    public static final int COLUMN_DELETED = 7;
    public static final int COLUMN_USERNAME = 8;
    public static final int COLUMN_UPDATED = 9;
    public static final int COLUMN_SYNCED = 10;

    private UsersCollection newUsers;
    private UsersCollection updatedUsers;
    private ArrayList<String> deletedUsers;
    private Response<UsersCollection> response;

    public UsersSyncManager(final Context context, Retrofit client, PinitApiInterface service, List<ContentProviderOperation> batch) {
        super(context, client, service, batch);
        // Build hash table of incoming entries
        //build a collection of new local entries
        newUsers = new UsersCollection();
        updatedUsers = new UsersCollection();
        deletedUsers = new ArrayList<>();
    }

    public void syncNewEntries(final SyncResult syncResult) throws IOException {

        SessionManager sm = SessionManager.getInstance(mContext);

        //todo fetch last sync time
        Call<UsersCollection> call = service.getUsers(sm.getSessionApiKey(), sm.getSyncTimestamp(FeedContract.Note.TABLE_NAME));

        response = call.execute();

        if (!response.isSuccess()){
            //todo think of a proper way to handle all status codes
            throw new IOException("Network request error code" + response.code());
        }

        if (response.body() == null)
            return;
        
        HashMap<String, User> entryMap = new HashMap<String, User>();
        for (User e : response.body().users) {
            entryMap.put(e.api_key, e);
        }

        // Get list of all items
        Log.i(this.getClass().toString(), "Fetching local entries for merge");
        Uri uri = FeedContract.User.CONTENT_URI; // Get all entries

        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        if (c != null) {
            Log.i(this.getClass().toString(), "Found " + c.getCount() + " local entries. Computing merge solution...");

            // Find stale data
            String api_key;
            String email;
            String name;
            String surname;
            String picture;
            int deleted;
            String username;
            int synced;
            long date_updated;

            while (c.moveToNext()) {
                syncResult.stats.numEntries++;
                // fetch all data
                api_key = c.getString(COLUMN_API_KEY);
                email = c.getString(COLUMN_EMAIL);
                name = c.getString(COLUMN_NAME);
                surname = c.getString(COLUMN_SURNAME);
                picture = c.getString(COLUMN_PICTURE);
                deleted = c.getInt(COLUMN_DELETED);
                username = c.getString(COLUMN_USERNAME);
                date_updated = c.getLong(COLUMN_UPDATED);
                synced = c.getInt(COLUMN_SYNCED);
                User match = entryMap.get(api_key);
                if (match != null) {
                    // Entry exists. Update it and remove from Map to prevent duplicate inserts.
                    entryMap.remove(api_key);

                    Uri existingUri = buildUri(api_key);
                    //ignore an already deleted entry
                    if (deleted != FeedContract.SyncableItem.DELETED) {
                        // Update existing record
                        // todo, removed optimiziation if no change is detected, see if we need it
                        Log.i(this.getClass().toString(), "Scheduling update: " + existingUri);
                        batch.add(ContentProviderOperation.newUpdate(existingUri)
                                .withValue(FeedContract.User.COLUMN_NAME_EMAIL, match.email)
                                .withValue(FeedContract.User.COLUMN_NAME_NAME, match.name)
                                .withValue(FeedContract.User.COLUMN_NAME_SURNAME, match.name)
                                .withValue(FeedContract.User.COLUMN_NAME_PICTURE, match.picture)
                                .withValue(FeedContract.User.COLUMN_NAME_DELETED, match.deleted)
                                .withValue(FeedContract.User.COLUMN_NAME_USERNAME, match.username)
                                .withValue(FeedContract.User.COLUMN_NAME_SYNCED, FeedContract.User.SYNC_STATUS_SYNCED)
                                .build());
                        syncResult.stats.numUpdates++;
                    }

                    // non matching, but "dirty" with CREATED flag set, add to the newCollection
                } else if (synced == FeedContract.User.SYNC_STATUS_CREATED) {
                    newUsers.add(new User(email, name, surname, picture, deleted, username, date_updated));
                    // non matching, but "dirty" with DIRTY flag set, add to the updatedCollection
                } else if (synced == FeedContract.User.SYNC_STATUS_UPDATED) {
                    updatedUsers.add(new User(email, name, surname, picture, deleted, username, date_updated));
                    // non matching, but "dirty" with DELETED flag set, add to the deletedCollection
                } else if (synced == FeedContract.User.SYNC_STATUS_DELETED) {
                    deletedUsers.add(api_key);
                }
            }
            c.close();
        }
        // Add fetched non existing entries to database
        for (User user : entryMap.values()) {
            Log.i(this.getClass().toString(), "Scheduling insert: entry_id=" + user.api_key);
            batch.add(ContentProviderOperation.newInsert(FeedContract.User.CONTENT_URI)
                    .withValue(FeedContract.User.COLUMN_NAME_EMAIL, user.email)
                    .withValue(FeedContract.User.COLUMN_NAME_NAME, user.name)
                    .withValue(FeedContract.User.COLUMN_NAME_SURNAME, user.surname)
                    .withValue(FeedContract.User.COLUMN_NAME_PICTURE, user.picture)
                    .withValue(FeedContract.User.COLUMN_NAME_DELETED, user.deleted)
                    .withValue(FeedContract.User.COLUMN_NAME_USERNAME, user.username)
                    .withValue(FeedContract.User.COLUMN_NAME_SYNCED, FeedContract.User.SYNC_STATUS_SYNCED)
                    .build());
            syncResult.stats.numInserts++;
        }

    }

    public void syncCreatedEntries(final SyncResult syncResult) throws IOException {

        throw new UnsupportedOperationException("Creating users is not supported!");
    }

    public void syncUpdatedEntries(final SyncResult syncResult) throws IOException {

        throw new UnsupportedOperationException("Updating users is not supported!");
    }

    public void syncDeletedEntries(final SyncResult syncResult) throws IOException {

        throw new UnsupportedOperationException("Deleting users is not supported!");

    }

    @Override
    protected Uri buildUri(String id) {
        return FeedContract.User.CONTENT_URI.buildUpon().appendPath(id).build();
    }

}