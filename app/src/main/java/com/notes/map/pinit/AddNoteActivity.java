package com.notes.map.pinit;

import android.content.ContentProviderOperation;
import android.content.OperationApplicationException;
import android.graphics.Color;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.Button;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.SimpleCursorAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.models.GeoNote;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.models.TodoNote;
import com.notes.map.pinit.basicsyncadapter.models.UserNote;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.forms.ItemForm;
import com.notes.map.pinit.services.ModelRepository;
import com.notes.map.pinit.services.SessionManager;
import com.pes.androidmaterialcolorpickerdialog.ColorPicker;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

/**
 * Created by iamkr on 25.1.2016..
 */
public class AddNoteActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    private String mNoteType, mGeoAddress;
    private double mLatitude, mLongitude;

    // flag for signalling if address was passed to this Activity
    private boolean addressPassed = false;

    //view controls
    @InjectView(R.id.button_toolbar) ImageButton mSaveButton;
    @InjectView(R.id.button_color_picker) ImageButton mColorButton;
    @InjectView(R.id.button_add_todo) @Optional ImageButton mAddButton;
    @InjectView(R.id.detailed_layout) @Optional LinearLayout mLinearLayout;

    @InjectView(R.id.spinner_label) Spinner mSpinner;

    //geo views
    @InjectView(R.id.geo_desc) @Optional EditText mEditTextDesc;
    @InjectView(R.id.geo_address) @Optional EditText mEditTextAddress;

    //note and todoNote views
    @InjectView(R.id.notes_title) @Optional EditText mNoteTitle;
    @InjectView(R.id.notes_desc) @Optional EditText mNoteDesc;

    private LinearLayout mParentLayout;

    private Label mSelectedLabel;

    private List<ItemForm> mTodos;

    private ColorPicker cp;
    private Button mOkColorButton;
    private int mIntColor;
    private String mHexColor;

    private Cursor labelCursor;

    @Override
    protected void onDestroy() {
        super.onDestroy();
        labelCursor.close();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);

        mNoteType = getIntent().getStringExtra("NOTE_TYPE");

        mGeoAddress = getIntent().getStringExtra("GEO_ADDRESS");
        mLatitude = getIntent().getDoubleExtra("GEO_LAT", -1);
        mLongitude = getIntent().getDoubleExtra("GEO_LON", -1);
        if (!TextUtils.isEmpty(mGeoAddress) && mLatitude != -1 && mLongitude != -1)
            addressPassed = true;

        switch (mNoteType) {
            case MainActivity.TYPE_NOTES:
                ViewStub stub = (ViewStub) findViewById(R.id.stub_note);
                stub.setLayoutResource(R.layout.notes);
                stub.inflate();
                break;
            case MainActivity.TYPE_TODO:
                stub = (ViewStub) findViewById(R.id.stub_todo);
                stub.setLayoutResource(R.layout.todos);
                stub.inflate();
                mTodos = new ArrayList<>();
                mParentLayout = (LinearLayout) findViewById(R.id.todo_root_layout);
                ItemForm defaultForm = new ItemForm(this, mParentLayout, new Item());
                mTodos.add(defaultForm);
                break;
            case MainActivity.TYPE_GEO:
                stub = (ViewStub) findViewById(R.id.stub_geo);
                stub.setLayoutResource(R.layout.geo_note);
                stub.inflate();
                break;
        }

        ButterKnife.inject(this);

        if (mAddButton != null)
            mAddButton.setOnClickListener(this);
        if (mEditTextAddress != null) {
            mEditTextAddress.setText(mGeoAddress);
            mEditTextAddress.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    // request new update of coordinates from MapsAPI when saving this note
                    addressPassed = false;
                }
            });
        }
        cp = new ColorPicker(AddNoteActivity.this, 255, 255, 255);
        mHexColor = "#FFFFFF";

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar2);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSaveButton.setImageResource(R.drawable.ic_save);
        mSaveButton.setOnClickListener(this);
        mColorButton.setOnClickListener(this);


        //todo use CursorLoader to manage the lifecycle of this Cursor, as SimpleCursorAdapter is deprecated
        labelCursor = ModelRepository.getInstance(getApplicationContext()).findLabels();
        SimpleCursorAdapter labelAdapter = new SimpleCursorAdapter(this,
                android.R.layout.simple_spinner_item,
                labelCursor,
                new String[]{FeedContract.Label.COLUMN_NAME_NAME},
                new int[]{android.R.id.text1});
        labelAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(labelAdapter);
        mSpinner.setOnItemSelectedListener(this);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.button_toolbar: //save button action
                List<ContentProviderOperation> batch;
                UserNote userNote;
                try {
                    if (mNoteType.equals(MainActivity.TYPE_NOTES)) {
                        Note note = new Note(mNoteTitle.getText().toString(), 0, mNoteDesc.getText().toString(), 0, mHexColor, FeedContract.Note.STATUS_NORMAL, null);
                        // add a label if selected
                        if (mSelectedLabel != null)
                            note.addLabel(mSelectedLabel);
                        // save a UserNote for the same note
                        userNote = new UserNote(note.uuid, SessionManager.getInstance(getApplicationContext()).getSessionApiKey(), 1, 1, 1);
                        // first make a Note save operation
                        // then UserNote and commit everything
                        batch = note.save(getApplicationContext());
                        batch.addAll(userNote.save(getApplicationContext()));
                        note.commit(batch);
                    } else if (mNoteType.equals(MainActivity.TYPE_TODO)) {
                        //todo add alarm
                        TodoNote mNote = new TodoNote(mNoteTitle.getText().toString(), 0, 0, mHexColor, FeedContract.Note.STATUS_NORMAL, null);
                        for (ItemForm item : mTodos) {
                            if (item.isValid())
                                mNote.addTodo(item.getModel());
                            else
                                mNote.removeTodo(item.getModel());
                        }
                        if (mSelectedLabel != null)
                            mNote.addLabel(mSelectedLabel);
                        userNote = new UserNote(mNote.uuid, SessionManager.getInstance(getApplicationContext()).getSessionApiKey(), 1, 1, 1);
                        batch = mNote.save(getApplicationContext());
                        batch.addAll(userNote.save(getApplicationContext()));
                        mNote.commit(batch);
                    } else if (mNoteType.equals(MainActivity.TYPE_GEO)){
                        // create a GeoNote
                        //todo add alarm
                        GeoNote mNote = new GeoNote(mNoteTitle.getText().toString(),
                                mEditTextDesc.getText().toString(), 0, 0, mHexColor, null);
                        if (mSelectedLabel != null)
                            mNote.addLabel(mSelectedLabel);
                        userNote = new UserNote(mNote.uuid, SessionManager.getInstance(getApplicationContext()).getSessionApiKey(), 1, 1, 1);
                        // create a Geo
                        if (!addressPassed){
                            Geocoder geocoder = new Geocoder(getApplicationContext());
                            List<Address> address;
                            //todo think of a way to move this to a non UI thread and show a progess dialog
                            try {
                                address = geocoder.getFromLocationName(mEditTextAddress.getText().toString(), 1);
                                if (address == null || address.size() == 0) {
                                    mEditTextAddress.setError("Invalid adress! No results found!");
                                    return;
                                }
                                Address location = address.get(0);
                                mLatitude = location.getLatitude();
                                mLongitude = location.getLongitude();
                            } catch (IOException e) {
                                Toast.makeText(AddNoteActivity.this, "Error getting coordinates! Check your network connection!", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                                return;
                            }
                        }
                        Geo geo = new Geo(mEditTextAddress.getText().toString(), mLongitude, mLatitude, 200);
                        mNote.addGeo(geo);
                        batch = mNote.save(getApplicationContext());
                        batch.addAll(userNote.save(getApplicationContext()));
                        mNote.commit(batch);
                        MapTab.addMarkersFromGeoNotes(getApplicationContext());
                    }
                } catch (RemoteException | OperationApplicationException e) {
                    Toast.makeText(getBaseContext(), "Failed to save Note!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
                finish();
                break;
            case R.id.button_add_todo:
                mTodos.add(new ItemForm(this, mParentLayout, new Item()));
                break;
            case R.id.button_color_picker:
                cp.show();
                mOkColorButton = (Button) cp.findViewById(R.id.okColorButton);
                mOkColorButton.setOnClickListener(this);
                break;
            case R.id.okColorButton:
                mIntColor = cp.getColor();
                mHexColor = String.format("#%06X", (0xFFFFFF & mIntColor));
                mLinearLayout.setBackgroundColor(mIntColor);
                cp.dismiss();
                break;
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        Cursor c = (Cursor) adapterView.getAdapter().getItem(i);
        mSelectedLabel = Label.fromCursor(c);
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {
        mSelectedLabel = null;
    }
}
