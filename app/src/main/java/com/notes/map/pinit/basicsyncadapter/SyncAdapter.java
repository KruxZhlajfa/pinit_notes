/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.notes.map.pinit.basicsyncadapter;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SyncResult;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.notes.map.pinit.BuildConfig;
import com.notes.map.pinit.basicsyncadapter.collections.UsersNotesCollection;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.basicsyncadapter.sync.GeosSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.ItemsSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.LabelsSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.NotesSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.UserNotesSyncManager;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.GsonConverterFactory;
import retrofit2.Retrofit;

/**
 * Define a sync adapter for the app.
 *
 * <p>This class is instantiated in {@link SyncService}, which also binds SyncAdapter to the system.
 * SyncAdapter should only be initialized in SyncService, never anywhere else.
 *
 * <p>The system calls onPerformSync() via an RPC call through the IBinder object supplied by
 * SyncService.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String TAG = "SyncAdapter";

    /**
     * URL to fetch content from during a sync.
     *
     * <p>This points to the Android Developers Blog. (Side note: We highly recommend reading the
     * Android Developer Blog to stay up to date on the latest Android platform developments!)
     */
    //todo ovo ce biti nas endpoint
    public static final String FEED_URL = "http://46.101.151.125";//"http://private-fc905-notesapi11.apiary-mock.com";

    /**
     * Network connection timeout, in milliseconds.
     */
    private static final int NET_CONNECT_TIMEOUT_MILLIS = 15000;  // 15 seconds

    /**
     * Network read timeout, in milliseconds.
     */
    private static final int NET_READ_TIMEOUT_MILLIS = 10000;  // 10 seconds

    /**
     * Content resolver, for performing database operations.
     */
    //todo trebat ce nam jedan ContentResolver
    private final ContentResolver mContentResolver;

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
        Log.d(TAG, "Created an instance of SyncAdapter!");
    }

    /**
     * Called by the Android system in response to a request to run the sync adapter. The work
     * required to read data from the network, parse it, and store it in the content provider is
     * done here. Extending AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter
     * run on a background thread. For this reason, blocking I/O and other long-running tasks can be
     * run <em>in situ</em>, and you don't have to set up a separate thread for them.
     .
     *
     * <p>This is where we actually perform any work required to perform a sync.
     * {@link AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread,
     * so it is safe to peform blocking I/O here.
     *
     * <p>The syncResult argument allows you to pass information back to the method that triggered
     * the sync.
     */
    //todo ovdje ce trebati implementirati algoritam sinkronizacije -> GET svih tablica koje treba
    //updateat, POST svih novih zapisa, PUT svih updeateanih zapisa, DELETE obrisanih zapisa
    //i oznacavati zapise sinkroniziranima kada dobije odgovor od apija
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
                              ContentProviderClient provider, SyncResult syncResult) {

        Log.d(TAG, "Beginning network synchronization");
        try {
            final URL location = new URL(FEED_URL);

            Log.d(TAG, "Streaming data from network: " + location);
            //todo downloadUrl metoda ce morati koristiti REST klijent za dohvacanje podataka
            updateLocalFeedData(syncResult);
            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (MalformedURLException e) {
            Log.e(TAG, "Feed URL is malformed", e);
            syncResult.stats.numParseExceptions++;
            return;
        } catch (IOException e) {
            Log.e(TAG, "Error reading from network: " + e.toString());
            syncResult.stats.numIoExceptions++;
            return;
        } catch (RemoteException | OperationApplicationException e) {
            Log.e(TAG, "Error updating database: " + e.toString());
            syncResult.databaseError = true;
            return;
        }
        Log.i(TAG, "Network synchronization complete");
    }

    /**
     * Read XML from an input stream, storing it into the content provider.
     *
     * <p>This is where incoming data is persisted, committing the results of a sync. In order to
     * minimize (expensive) disk operations, we compare incoming data with what's already in our
     * database, and compute a merge. Only changes (insert/update/delete) will result in a database
     * write.
     *
     * <p>As an additional optimization, we use a batch operation to perform all database writes at
     * once.
     *
     * <p>Sync strategy:
     * For each item:
     *   1. Get cursor to all items <br/>
     *   2. For each item, check if it's in the incoming data.<br/>
     *    a. YES: Check timestamp and perform database UPDATE if greater. Ignore if already deleted locally<br/>
     *    b. NO: check local status of item <br/>
     *       b0. item CREATED - mark it for CREATE on server
     *       b1. item UPDATED - mark it for UPDATE on server
     *       b2. item DELETED - mark it for DELETE on server
     *   3. Iterate over CREATED, UPDATED, DELETED lists and send to server
     *   (At this point, database contains synced state as it was at the time of last fetch)<br/>
     */
    public void updateLocalFeedData(final SyncResult syncResult)
            throws IOException, RemoteException, OperationApplicationException {

        //initialize GSON
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().serializeNulls().create();

        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.readTimeout(10, TimeUnit.SECONDS);
        builder.connectTimeout(5, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.addInterceptor(interceptor);
        }
        OkHttpClient httpClient = builder.build();

        //initialize client
        Retrofit client = new Retrofit.Builder()
                .baseUrl(SyncAdapter.FEED_URL)
                .client(httpClient)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        PinitApiInterface service = client.create(PinitApiInterface.class);

        ArrayList<ContentProviderOperation> batch = new ArrayList<>();

        LabelsSyncManager labelsSyncManager = new LabelsSyncManager(getContext(), client, service, batch);
        labelsSyncManager.syncNewEntries(syncResult);
        labelsSyncManager.syncCreatedEntries(syncResult);
        labelsSyncManager.syncUpdatedEntries(syncResult);
        labelsSyncManager.syncDeletedEntries(syncResult);

        GeosSyncManager geosSyncManager = new GeosSyncManager(getContext(), client, service, batch);
        geosSyncManager.syncNewEntries(syncResult);
        geosSyncManager.syncCreatedEntries(syncResult);
        geosSyncManager.syncUpdatedEntries(syncResult);
        geosSyncManager.syncDeletedEntries(syncResult);

        NotesSyncManager notesSyncManager = new NotesSyncManager(getContext(), client, service, batch);
        notesSyncManager.syncNewEntries(syncResult);
        notesSyncManager.syncCreatedEntries(syncResult);
        notesSyncManager.syncUpdatedEntries(syncResult);
        notesSyncManager.syncDeletedEntries(syncResult);

        ItemsSyncManager itemsSyncManager = new ItemsSyncManager(getContext(), client, service, batch);
        itemsSyncManager.syncNewEntries(syncResult);
        itemsSyncManager.syncCreatedEntries(syncResult);
        itemsSyncManager.syncUpdatedEntries(syncResult);
        itemsSyncManager.syncDeletedEntries(syncResult);

        UserNotesSyncManager usersNotesSyncManager = new UserNotesSyncManager(getContext(), client, service, batch);
        usersNotesSyncManager.syncNewEntries(syncResult);
        usersNotesSyncManager.syncCreatedEntries(syncResult);


        //todo update drugih tablica, post, put, delete etc.
        Log.i(TAG, "Merge solution ready. Applying batch update");
        mContentResolver.applyBatch(FeedContract.CONTENT_AUTHORITY, batch);
        mContentResolver.notifyChange(
                FeedContract.Label.CONTENT_URI, // URI where data was modified
                null,                           // No local observer
                false);                         // IMPORTANT: Do not sync to network
        mContentResolver.notifyChange(
                FeedContract.Geo.CONTENT_URI, // URI where data was modified
                null,                           // No local observer
                false);                         // IMPORTANT: Do not sync to network
        mContentResolver.notifyChange(
                FeedContract.Note.CONTENT_URI, // URI where data was modified
                null,                           // No local observer
                false);                         // IMPORTANT: Do not sync to network
        mContentResolver.notifyChange(
                FeedContract.Item.CONTENT_URI, // URI where data was modified
                null,                           // No local observer
                false);                         // IMPORTANT: Do not sync to network
        mContentResolver.notifyChange(
                FeedContract.UserNote.CONTENT_URI,
                null);
    }

}
