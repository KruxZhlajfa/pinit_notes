package com.notes.map.pinit.basicsyncadapter.sync;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.collections.LabelsCollection;
import com.notes.map.pinit.basicsyncadapter.collections.NotesCollection;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Antonio on 23.1.2016..
 */
public class LabelsSyncManager extends AbstractSyncManager {

    /**
     * Projection used when querying content provider. Returns all known fields.
     */
    public static final String[] PROJECTION = new String[]{
            FeedContract.Label._ID,
            FeedContract.Label.COLUMN_NAME_NAME,
            FeedContract.Label.COLUMN_NAME_UPDATED,
            FeedContract.Label.COLUMN_NAME_DELETED,
            FeedContract.Label.COLUMN_NAME_UUID,
            FeedContract.Label.COLUMN_NAME_USERS_API_KEY,
            FeedContract.Label.COLUMN_NAME_SYNCED
    };

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
    public static final int COLUMN_NAME = 1;
    public static final int COLUMN_UPDATED = 2;
    public static final int COLUMN_DELETED = 3;
    public static final int COLUMN_UUID = 4;
    public static final int COLUMN_USERS_API_KEY = 5;
    public static final int COLUMN_SYNCED = 6;

    private LabelsCollection newLabels;
    private LabelsCollection updatedLabels;
    private List<String> deletedLabels;
    private Response<LabelsCollection> response;

    public LabelsSyncManager(final Context context, Retrofit client, PinitApiInterface service, List<ContentProviderOperation> batch) {
        super(context, client, service, batch);
        //build a collection of local entries
        newLabels = new LabelsCollection();
        updatedLabels = new LabelsCollection();
        deletedLabels = new ArrayList<String>();

    }

    public void syncNewEntries(final SyncResult syncResult) throws IOException {

        Call<LabelsCollection> call = service.getLabels(
                SessionManager.getInstance(mContext).getSessionApiKey(),
                SessionManager.getInstance(mContext).getSyncTimestamp(FeedContract.Label.TABLE_NAME));

        response = call.execute();

        if (response.body() == null) {
            Log.e(getClass().toString(), "Error syncing New entries, response body was null!");
            return;
        }

        if (!response.isSuccess()){
            //todo think of a proper way to handle all status codes
            throw new IOException("Network request error code" + response.code());
        }

        HashMap<String, Label> entryMap = new HashMap<>();
        for (Label e : response.body().labels) {
            entryMap.put(e.uuid, e);
        }

        // Get list of all items
        Log.i(this.getClass().toString(), "Fetching local labels for merge");
        Uri uri = FeedContract.Label.CONTENT_URI; // Get all entries
        Log.d(this.getClass().toString(), "URI" + uri);
        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        if ( c != null ) {
            Log.i(this.getClass().toString(), "Found " + c.getCount() + " local entries. Computing merge solution...");

            // Find stale data
            String uuid;
            String name;
            long updated;
            int deleted;
            String users_api_key;
            int synced;

            while (c.moveToNext()) {
                syncResult.stats.numEntries++;
                // fetch all data
                uuid = c.getString(COLUMN_UUID);
                name = c.getString(COLUMN_NAME);
                updated = c.getLong(COLUMN_UPDATED);
                deleted = c.getInt(COLUMN_DELETED);
                users_api_key = c.getString(COLUMN_USERS_API_KEY);
                synced = c.getInt(COLUMN_SYNCED);
                Label match = entryMap.get(uuid);
                if (match != null) {
                    // Entry exists. Update it and remove from Map to prevent duplicate inserts.
                    entryMap.remove(uuid);

                    Uri existingUri = buildUri(uuid);
                    //ignore an already stale entry or an already deleted entry
                    if (match.date_updated > updated && match.deleted != FeedContract.SyncableItem.DELETED && deleted != FeedContract.SyncableItem.DELETED) {
                        // Update existing record
                        Log.i(this.getClass().toString(), "Scheduling update: " + existingUri);
                        updateLabel(existingUri, match);
                        syncResult.stats.numUpdates++;
                    } else if (match.deleted == FeedContract.SyncableItem.DELETED) {
                        // Delete local record from database
                        batch.add(ContentProviderOperation.newDelete(existingUri).build());
                        syncResult.stats.numDeletes++;
                    } else if (updated > match.date_updated && deleted != FeedContract.SyncableItem.DELETED){
                        //local id is newer, update it on server
                        updatedLabels.add(new Label(uuid, name, users_api_key, updated, deleted));
                    } else if (deleted == FeedContract.SyncableItem.DELETED) {
                        //deleted locally, delete on server
                        deletedLabels.add(uuid);
                    }

                    // non matching, but "dirty" with CREATED flag set, add to the newCollection
                } else if (synced == FeedContract.Label.SYNC_STATUS_CREATED) {
                    newLabels.add(new Label(uuid, name, users_api_key, updated, deleted));
                    // non matching, but "dirty" with DIRTY flag set, add to the updatedCollection
                } else if (synced == FeedContract.Label.SYNC_STATUS_UPDATED) {
                    updatedLabels.add(new Label(uuid, name, users_api_key, updated, deleted));
                    // non matching, but "dirty" with DELETED flag set, add to the deletedCollection
                } else if (synced == FeedContract.Label.SYNC_STATUS_DELETED) {
                    deletedLabels.add(uuid);
                }
            }
            c.close();
        }
        // Add fetched non existing entries to database
        for (Label label : entryMap.values()) {
            Log.i(this.getClass().toString(), "Scheduling insert: entry_id=" + label.uuid);
            batch.add(ContentProviderOperation.newInsert(FeedContract.Label.CONTENT_URI)
                    .withValue(FeedContract.Label.COLUMN_NAME_NAME, label.name)
                    .withValue(FeedContract.Label.COLUMN_NAME_UPDATED, label.date_updated)
                    .withValue(FeedContract.Label.COLUMN_NAME_DELETED, label.deleted)
                    .withValue(FeedContract.Label.COLUMN_NAME_USERS_API_KEY, label.users_api_key)
                    .withValue(FeedContract.Label.COLUMN_NAME_SYNCED, FeedContract.Label.SYNC_STATUS_SYNCED)
                    .withValue(FeedContract.Label.COLUMN_NAME_UUID, label.uuid)
                    .build());
            syncResult.stats.numInserts++;
        }

    }

    public void syncCreatedEntries(final SyncResult syncResult) throws IOException {
        // Let's POST all the new "dirty" entries (that have status = CREATED)
        /*LabelsCollection lcol = new LabelsCollection();*/

        Gson gson = new Gson();

        NotesCollection lcol = new NotesCollection();
        List list = new ArrayList();
        list.add("Neki error");
        list.add("Neki error 2");
        Map map = new HashMap();
        map.put("12345678", list);
        map.put("1325", list);
        lcol.errors = map;
        Log.d("JSONDEBUG", gson.toJson(lcol));

        Log.d("JSONDEBUG", gson.toJson(newLabels));
        if (newLabels.size() > 0) {
            Call<LabelsCollection> createdCall = service.createLabels(SessionManager.getInstance(mContext).getSessionApiKey(), newLabels);
            response = createdCall.execute();

            if (response == null){
                Log.e(getClass().toString(), "Error syncing Created entries, response body was null!");
                return;
            }

            logErrors(response.body().errors);

            // conflict detected, solve by saving conflicts to local as needed
            for (Label label : (List<Label>)response.body().conflicts) {
                Uri existingUri = buildUri(label.uuid);
                Label oldLabel = newLabels.get(label.uuid);
                // check timestamp
                if (oldLabel.date_updated < label.date_updated){
                    label._id = oldLabel._id;
                    updateLabel(existingUri, label);
                } else {
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Label.COLUMN_NAME_SYNCED, FeedContract.Label.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
            // check sucessfully created notes and mark them
            for (Label label : response.body().labels) {
                Uri existingUri = buildUri(label.uuid);
                // we mark it synced
                batch.add(ContentProviderOperation.newUpdate(existingUri)
                        .withValue(FeedContract.Label.COLUMN_NAME_SYNCED, FeedContract.Label.SYNC_STATUS_SYNCED)
                        .build());
            }
        }
    }

    public void syncUpdatedEntries(final SyncResult syncResult) throws IOException {
        // Let's PUT all the updated entries
        // todo think of a correct way to solve (unlikely) conflicts
        // todo try refactoring into a generic method using metaprogramming, to stay DRY
        if (updatedLabels.size() > 0) {
            Call<LabelsCollection> updatedCall = service.updateLabels(SessionManager.getInstance(mContext).getSessionApiKey(), updatedLabels);
            response = updatedCall.execute();
            if(response.isSuccess()) {
                for (Label label : response.body().labels) {
                    Uri existingUri = buildUri(label.uuid);
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Label.COLUMN_NAME_SYNCED, FeedContract.Label.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
        }
    }

    public void syncDeletedEntries(final SyncResult syncResult) throws IOException {

        // Let's DELETE all the deleted entries
        if (deletedLabels.size() > 0) {
            for (String uuid : deletedLabels) {
                Call<Label> deletedCall = service.deleteLabels(SessionManager.getInstance(mContext).getSessionApiKey(), uuid);
                Response<Label> response = deletedCall.execute();
                if (response.isSuccess()) {
                    Uri existingUri = buildUri(uuid);
                    batch.add(ContentProviderOperation.newDelete(existingUri)
                            .build());
                }
            }
        }

    }

    @Override
    protected Uri buildUri(String id) {
        return FeedContract.Label.CONTENT_URI.buildUpon().appendPath(id).build();
    }

    private void updateLabel(Uri existingUri, Label match){
        batch.add(ContentProviderOperation.newUpdate(existingUri)
                .withValue(FeedContract.Label.COLUMN_NAME_NAME, match.name)
                .withValue(FeedContract.Label.COLUMN_NAME_UPDATED, match.date_updated)
                .withValue(FeedContract.Label.COLUMN_NAME_DELETED, match.deleted)
                .withValue(FeedContract.Label.COLUMN_NAME_USERS_API_KEY, match.users_api_key)
                .withValue(FeedContract.Label.COLUMN_NAME_SYNCED, FeedContract.Label.SYNC_STATUS_SYNCED)
                .build());
    }
}
