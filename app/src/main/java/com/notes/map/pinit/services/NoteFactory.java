package com.notes.map.pinit.services;

import com.notes.map.pinit.basicsyncadapter.models.GeoNote;
import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.models.TodoNote;

import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

/**
 * Created by Antonio on 28.1.2016..
 */
public class NoteFactory {

    public static Note createNote(int type) throws Exception {
        Note note;
        switch (type){
            case FeedContract.Note.TYPE_NORMAL :
                note = new Note();
                break;
            case FeedContract.Note.TYPE_TODO:
                note = new TodoNote();
                break;
            case FeedContract.Note.TYPE_GEO:
                note = new GeoNote();
                break;
            default:
                throw new Exception("Non existent Note type!");
        }
        return note;
    }
}
