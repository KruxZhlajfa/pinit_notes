package com.notes.map.pinit.basicsyncadapter.sync;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.collections.NotesCollection;
import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Antonio on 23.1.2016..
 *
 * A class for executing the sync algorithm for the Notes table
 *
 */
public class NotesSyncManager extends AbstractSyncManager {

    /**
     * Projection used when querying content provider. Returns all known fields.
     */
    public static final String[] PROJECTION = new String[]{
            FeedContract.Note._ID,
            FeedContract.Note.COLUMN_NAME_TITLE,
            FeedContract.Note.COLUMN_NAME_CREATED,
            FeedContract.Note.COLUMN_NAME_UPDATED,
            FeedContract.Note.COLUMN_NAME_ALARM,
            FeedContract.Note.COLUMN_NAME_TEXT,
            FeedContract.Note.COLUMN_NAME_PRIORITY,
            FeedContract.Note.COLUMN_NAME_COLOR,
            FeedContract.Note.COLUMN_NAME_STATUS,
            FeedContract.Note.COLUMN_NAME_UUID,
            FeedContract.Note.COLUMN_NAME_LABELS_ID,
            FeedContract.Note.COLUMN_NAME_GEOS_ID,
            FeedContract.Note.COLUMN_NAME_TYPE,
            FeedContract.Note.COLUMN_NAME_SYNCED
    };

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
    public static final int COLUMN_TITLE = 1;
    public static final int COLUMN_CREATED = 2;
    public static final int COLUMN_UPDATED = 3;
    public static final int COLUMN_ALARM = 4;
    public static final int COLUMN_TEXT = 5;
    public static final int COLUMN_PRIORITY = 6;
    public static final int COLUMN_COLOR = 7;
    public static final int COLUMN_STATUS = 8;
    public static final int COLUMN_UUID = 9;
    public static final int COLUMN_LABELS_ID = 10;
    public static final int COLUMN_GEOS_ID = 11;
    public static final int COLUMN_TYPE = 12;
    public static final int COLUMN_SYNCED = 13;


    private NotesCollection newNotes;
    private NotesCollection updatedNotes;
    private ArrayList<String> deletedNotes;
    private Response<NotesCollection> response;

    public NotesSyncManager(final Context context, Retrofit client, PinitApiInterface service, List<ContentProviderOperation> batch) {
        super(context, client, service, batch);
        // Build hash table of incoming entries
        //build a collection of new local entries
        newNotes = new NotesCollection();
        updatedNotes = new NotesCollection();
        deletedNotes = new ArrayList<String>();

    }

    public void syncNewEntries(final SyncResult syncResult) throws IOException {
        Call<NotesCollection> call = service.getNotes(
                SessionManager.getInstance(mContext).getSessionApiKey(),
                SessionManager.getInstance(mContext).getSyncTimestamp(FeedContract.Note.TABLE_NAME));

        response = call.execute();

        if (!response.isSuccess()){
            //todo think of a proper way to handle all status codes
            throw new IOException("Network request error code" + response.code());
        }

        if (response.body() == null)
            return;

        HashMap<String, Note> entryMap = new HashMap<>();
        for (Note e : response.body().notes) {
            entryMap.put(e.uuid, e);
        }

        // Get list of all items
        Log.i(this.getClass().toString(), "Fetching local entries for merge");
        Uri uri = FeedContract.Note.CONTENT_URI; // Get all entries
        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        if ( c != null ) {
            Log.i(this.getClass().toString(), "Found " + c.getCount() + " local entries. Computing merge solution...");

            // Find stale data
            long id;
            String uuid;
            String title;
            long created;
            long updated;
            long alarm;
            String text;
            int priority;
            String color;
            int status;
            String labels_id;
            String geos_id;
            int type;
            int synced;

            while (c.moveToNext()) {
                syncResult.stats.numEntries++;
                // fetch all data
                id = c.getLong(COLUMN_ID);
                uuid = c.getString(COLUMN_UUID);
                title = c.getString(COLUMN_TITLE);
                created = c.getLong(COLUMN_CREATED);
                updated = c.getLong(COLUMN_UPDATED);
                alarm = c.getLong(COLUMN_ALARM);
                text = c.getString(COLUMN_TEXT);
                priority = c.getInt(COLUMN_PRIORITY);
                color = c.getString(COLUMN_COLOR);
                status = c.getInt(COLUMN_STATUS);
                labels_id = c.getString(COLUMN_LABELS_ID);
                geos_id = c.getString(COLUMN_GEOS_ID);
                type = c.getInt(COLUMN_TYPE);
                synced = c.getInt(COLUMN_SYNCED);
                Note match = entryMap.get(uuid);
                if (match != null) {
                    // Entry exists. Update it and remove from Map to prevent duplicate inserts.
                    entryMap.remove(uuid);

                    Uri existingUri = buildUri(uuid);
                    if (match.date_updated > updated && match.status != FeedContract.Note.STATUS_DELETED && status != FeedContract.Note.STATUS_DELETED) {
                        //ignore an already stale entry or an already deleted entry
                        // Update existing record
                        Log.i(this.getClass().toString(), "Scheduling update: " + existingUri);
                        updateNote(existingUri, match);
                        syncResult.stats.numUpdates++;
                    } else if (match.status == FeedContract.Note.STATUS_DELETED){
                        // server deleted the note, delete it locally
                        batch.add(ContentProviderOperation.newDelete(existingUri).build());
                        syncResult.stats.numDeletes++;
                    } else if (updated > match.date_updated && status != FeedContract.Note.STATUS_DELETED) {
                        // local is newer, update it on server
                        updatedNotes.add(new Note(id, uuid, title, created, updated, alarm, text, priority,color, status, labels_id, geos_id, type));
                    } else if (status == FeedContract.Note.STATUS_DELETED){
                        // deleted locally, server is not up to date, add to deletes
                        deletedNotes.add(uuid);
                    }
                    // non matching, but "dirty" with CREATED flag set, add to the newCollection
                } else if (synced == FeedContract.Note.SYNC_STATUS_CREATED) {
                    newNotes.add(new Note(id, uuid, title, created, updated, alarm, text, priority,color, status, labels_id, geos_id, type));
                    // non matching, but "dirty" with DIRTY flag set, add to the updatedCollection
                } else if (synced == FeedContract.Note.SYNC_STATUS_UPDATED) {
                    updatedNotes.add(new Note(id, uuid, title, created, updated, alarm, text, priority,color, status, labels_id, geos_id, type));
                    // non matching, but "dirty" with DELETED flag set, add to the deletedCollection
                } else if (synced == FeedContract.Note.SYNC_STATUS_DELETED) {
                    deletedNotes.add(uuid);
                }
            }
            c.close();
        }
        // Add fetched non existing entries to database
        for (Note note : entryMap.values()) {
            Log.i(this.getClass().toString(), "Scheduling insert: entry_id=" + note.uuid);
            batch.add(ContentProviderOperation.newInsert(FeedContract.Note.CONTENT_URI)
                    .withValue(FeedContract.Note.COLUMN_NAME_TITLE, note.title)
                    .withValue(FeedContract.Note.COLUMN_NAME_CREATED, note.date_created)
                    .withValue(FeedContract.Note.COLUMN_NAME_UPDATED, note.date_updated)
                    .withValue(FeedContract.Note.COLUMN_NAME_ALARM, note.date_alarm)
                    .withValue(FeedContract.Note.COLUMN_NAME_TEXT, note.text)
                    .withValue(FeedContract.Note.COLUMN_NAME_PRIORITY, note.priority)
                    .withValue(FeedContract.Note.COLUMN_NAME_STATUS, note.status)
                    .withValue(FeedContract.Note.COLUMN_NAME_LABELS_ID, note.labels_id)
                    .withValue(FeedContract.Note.COLUMN_NAME_GEOS_ID, note.geos_id)
                    .withValue(FeedContract.Note.COLUMN_NAME_UUID, note.uuid)
                    .withValue(FeedContract.Note.COLUMN_NAME_TYPE, note.type)
                    .withValue(FeedContract.Note.COLUMN_NAME_COLOR, note.color)
                    .withValue(FeedContract.Note.COLUMN_NAME_SYNCED, FeedContract.Note.SYNC_STATUS_SYNCED)
                    .build());
            syncResult.stats.numInserts++;
        }

    }

    public void syncCreatedEntries(final SyncResult syncResult) throws IOException {
        // Let's POST all the new "dirty" entries (that have status = CREATED)
        if (newNotes.size() > 0) {
            Call<NotesCollection> createdCall = service.createNotes(SessionManager.getInstance(mContext).getSessionApiKey(), newNotes);
            response = createdCall.execute();

            if (response.body() == null){
                Log.e(getClass().getSimpleName(), "Response body was null!");
                return;
            }

            logErrors(response.body().errors);

            // check conflicts, resolve by saving the received conflicted Notes to
            // local database
            for(Note note : (List<Note>)response.body().conflicts) {
                Uri existingUri = buildUri(note.uuid);
                // we put tne note status to normal and mark it synced
                // first, get local note id from the new notes Collection
                // todo see if it's better to fetch local id from db, Collection could be slow(er)?
                Note oldNote = newNotes.get(note.uuid);
                // check timestamp to see what needs to be done
                if (oldNote.date_updated < note.date_updated) {
                    // then put it into the received remote note
                    note._id = oldNote._id;
                    // overwrite old note with new
                    updateNote(existingUri, note);
                } else {
                    // note already exists on server, just mark it synced
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Note.COLUMN_NAME_SYNCED, FeedContract.Note.SYNC_STATUS_SYNCED)
                            .build());
                }
            }

            // check sucessfully created notes and mark them
            for (Note note : response.body().notes) {
                Uri existingUri = buildUri(note.uuid);
                // we mark it synced
                batch.add(ContentProviderOperation.newUpdate(existingUri)
                        .withValue(FeedContract.Note.COLUMN_NAME_SYNCED, FeedContract.Note.SYNC_STATUS_SYNCED)
                        .build());
            }
        }
    }

    public void syncUpdatedEntries(final SyncResult syncResult) throws IOException {
        // Let's PUT all the updated entries
        // todo think of a correct way to solve (very unlikely) conflicts
        // todo try refactoring into a generic method using metaprogramming, to stay DRY
        if (updatedNotes.size() > 0) {
            Call<NotesCollection> updatedCall = service.updateNotes(SessionManager.getInstance(mContext).getSessionApiKey(), updatedNotes);
            response = updatedCall.execute();
            if(response.isSuccess()) {
                for (Note note : response.body().notes) {
                    Uri existingUri = buildUri(note.uuid);
                    // we put tne note status to normal and mark it synced
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Note.COLUMN_NAME_SYNCED, FeedContract.Note.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
        }
    }

    public void syncDeletedEntries(final SyncResult syncResult) throws IOException {

        // Let's DELETE all the deleted entries
        if (deletedNotes.size() > 0) {
            for (String uuid : deletedNotes) {
                Call<Note> deletedCall = service.deleteNote(SessionManager.getInstance(mContext).getSessionApiKey(), uuid);
                Response<Note> response = deletedCall.execute();
                if (response.isSuccess()) {
                    Uri existingUri = buildUri(uuid);
                    batch.add(ContentProviderOperation.newDelete(existingUri)
                            .build());
                }
            }
        }

    }

    @Override
    protected Uri buildUri(String id) {
        return FeedContract.Note.CONTENT_URI.buildUpon().appendPath(id).build();
    }

    private void updateNote(Uri uri, Note note){
        batch.add(ContentProviderOperation.newUpdate(uri)
                .withValue(FeedContract.Note.COLUMN_NAME_TITLE, note.title)
                .withValue(FeedContract.Note.COLUMN_NAME_CREATED, note.date_created)
                .withValue(FeedContract.Note.COLUMN_NAME_UPDATED, note.date_updated)
                .withValue(FeedContract.Note.COLUMN_NAME_ALARM, note.date_alarm)
                .withValue(FeedContract.Note.COLUMN_NAME_TEXT, note.text)
                .withValue(FeedContract.Note.COLUMN_NAME_PRIORITY, note.priority)
                .withValue(FeedContract.Note.COLUMN_NAME_COLOR, note.color)
                .withValue(FeedContract.Note.COLUMN_NAME_STATUS, note.status)
                .withValue(FeedContract.Note.COLUMN_NAME_LABELS_ID, note.labels_id)
                .withValue(FeedContract.Note.COLUMN_NAME_GEOS_ID, note.geos_id)
                .withValue(FeedContract.Note.COLUMN_NAME_SYNCED, FeedContract.Note.SYNC_STATUS_SYNCED)
                .withValue(FeedContract.Note.COLUMN_NAME_TYPE, note.type)
                .build());
    }
}
