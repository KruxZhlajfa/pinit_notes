package com.notes.map.pinit;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.flipboard.bottomsheet.BottomSheetLayout;
import com.flipboard.bottomsheet.commons.MenuSheetView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.models.GeoNote;
import com.notes.map.pinit.services.ModelRepository;
import com.quinny898.library.persistentsearch.SearchBox;
import com.quinny898.library.persistentsearch.SearchResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapTab extends Fragment implements LocationListener {

    private static GoogleMap mMap;
    private LocationManager locationManager;
    private static final long MIN_TIME = 400;
    private static final float MIN_DISTANCE = 1000;
    private EditText mSearchQuery;
    private ImageButton mSearchButton;
    private SearchBox search;
    private ArrayList<Marker> mSearchMarkers;
    private static ArrayList<Marker> mGeoNoteMarkers;
    private BottomSheetLayout mBottomSheet;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.tab_map,container,false);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mMap = mapFragment.getMap();
        mMap.setMyLocationEnabled(true);
        mBottomSheet = (BottomSheetLayout) getActivity().findViewById(R.id.bottomsheet);
        mSearchMarkers = new ArrayList<Marker>();
        mGeoNoteMarkers = new ArrayList<Marker>();

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                ModelRepository repository = ModelRepository.getInstance(getActivity().getApplicationContext());
                final Cursor c = repository.findGeoNotes();
                for (int i = 0; i<mSearchMarkers.size(); i++){
                    if (marker.equals(mSearchMarkers.get(i))) {
                        showMenuSheet(MenuSheetView.MenuType.GRID, marker.getTitle(), marker.getPosition());
                    }
                }
                while (c.moveToNext()){
                    GeoNote mGeoNote = GeoNote.fromCursor(c);
                    if (mGeoNote.title.equals(marker.getTitle().toString()))
                        showGeoNoteMenuSheet(MenuSheetView.MenuType.GRID, mGeoNote.title, mGeoNote);
                }
                return false;
            }
        });

        locationManager = (LocationManager) getActivity().getSystemService(getContext().LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, MIN_TIME, MIN_DISTANCE, this);

        search = (SearchBox) v.findViewById(R.id.searchbox);
        initSearchBox(search);
        addMarkersFromGeoNotes(getActivity().getApplicationContext());
        return v;
    }

    public static void addMarkersFromGeoNotes(Context context){
        ModelRepository mRepository = ModelRepository.getInstance(context);
        Cursor mCursor = mRepository.findGeoNotes();

        while (mCursor.moveToNext()) {
            GeoNote mGeoNote = GeoNote.fromCursor(mCursor);
            Geo mGeo = mGeoNote.getGeo(context);
            String mNoteDesc = mGeoNote.text;

            LatLng latLng = new LatLng(mGeo.latitude, mGeo.longitude);
            if (mNoteDesc.length() > 50)
                mNoteDesc = mNoteDesc.substring(0, 50) + "...";

            MarkerOptions markerOptions = new MarkerOptions();
            markerOptions.position(latLng);
            markerOptions.title(mGeoNote.title);
            markerOptions.snippet(mNoteDesc);

            mMap.addMarker(markerOptions);
        }
    }

    private void showMenuSheet(final MenuSheetView.MenuType menuType, final String location, LatLng latLng){
        final double latitude = latLng.latitude;
        final double longitude = latLng.longitude;
        MenuSheetView menuSheetView =
                new MenuSheetView(getActivity(), menuType, location, new MenuSheetView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.item_sheet_geo_note){
                            Intent intentNote = new Intent(getActivity(), AddNoteActivity.class);
                            intentNote.putExtra("NOTE_TYPE", MainActivity.TYPE_GEO);
                            intentNote.putExtra("GEO_ADDRESS", location);
                            intentNote.putExtra("GEO_LAT", latitude);
                            intentNote.putExtra("GEO_LON", longitude);
                            startActivity(intentNote);
                        }
                        if (mBottomSheet.isSheetShowing()) {
                            mBottomSheet.dismissSheet();
                        }
                        return true;
                    }
                });
        menuSheetView.inflateMenu(R.menu.menu_btm_sheet);
        mBottomSheet.showWithSheetView(menuSheetView);
    }

    private void showGeoNoteMenuSheet(final MenuSheetView.MenuType menuType, final String title, GeoNote geoNote){
        final GeoNote mGeoNote = geoNote;
        MenuSheetView menuSheetView =
                new MenuSheetView(getActivity(), menuType, title, new MenuSheetView.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.item_sheet_geo_note){
                            Intent intent = new Intent(getActivity(), DetailedActivity.class);
                            intent.putExtra(DetailedActivity.ID, mGeoNote);
                            startActivity(intent);
                        }
                        if (mBottomSheet.isSheetShowing()) {
                            mBottomSheet.dismissSheet();
                        }
                        return true;
                    }
                });
        menuSheetView.inflateMenu(R.menu.menu_btm_sheet);
        MenuItem mItem = menuSheetView.getMenu().findItem(R.id.item_sheet_geo_note);
        mItem.setTitle("Open this geo note");
        mItem.setIcon(R.drawable.ic_pin);
        mBottomSheet.showWithSheetView(menuSheetView);
    }

    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(cameraUpdate);
        locationManager.removeUpdates(this);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) { }

    @Override
    public void onProviderEnabled(String provider) { }

    @Override
    public void onProviderDisabled(String provider) { }

    public void initSearchBox(final SearchBox mSearchBox){ //todo play with the different methods to see if you can make it look better
        mSearchBox.setHint("Search maps");
        mSearchBox.setMenuVisibility(View.INVISIBLE);
        mSearchBox.setLogoText("Search");
        mSearchBox.setDrawerLogo(R.drawable.ic_search);
        mSearchBox.setAnimateDrawerLogo(false);

        mSearchBox.setSearchListener(new SearchBox.SearchListener() {

            @Override
            public void onSearchOpened() {
                //Use this to tint the screen
            }

            @Override
            public void onSearchClosed() {
                //Use this to un-tint the screen
            }

            @Override
            public void onSearchTermChanged(String string) {
                //React to the search term changing
                //Called after it has updated results
            }

            @Override
            public void onSearch(String searchTerm) {
                search.showLoading(true);
                if(searchTerm!=null && !searchTerm.equals("")){
                    new GeocoderTask().execute(searchTerm);
                }
            }

            @Override
            public void onResultClick(SearchResult result) {
                //React to a result being clicked
            }


            @Override
            public void onSearchCleared() {

            }

        });
    }

    // An AsyncTask class for accessing the GeoCoding Web Service
    private class GeocoderTask extends AsyncTask<String, Void, List<Address>> {

        @Override
        protected List<Address> doInBackground(String... locationName) {
            // Creating an instance of Geocoder class
            Geocoder geocoder = new Geocoder(getActivity().getBaseContext());
            List<Address> addresses = null;

            try {
                // Getting a maximum of 3 Address that matches the input text
                addresses = geocoder.getFromLocationName(locationName[0], 3);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return addresses;
        }

        @Override
        protected void onPostExecute(List<Address> addresses) {

            if(addresses==null || addresses.size()==0){
                Toast.makeText(getActivity().getBaseContext(), "No results found", Toast.LENGTH_SHORT).show();
                return;
            }

            // Clears all the existing markers on the map
            mMap.clear();

            // Clears previous searched markers
            if (!mSearchMarkers.isEmpty())
                mSearchMarkers.clear();

            // Adding Markers on Google Map for each matching address
            for(int i=0;i<addresses.size();i++){

                Address address = (Address) addresses.get(i);

                // Creating an instance of GeoPoint, to display in Google Map
                LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());

                String addressText = String.format("%s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getCountryName());

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title(addressText);

                Marker marker = mMap.addMarker(markerOptions);
                mSearchMarkers.add(marker);

                // Locate the first location
                if(i==0){
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
                    search.showLoading(false);
                }

            }
        }
    }
}

