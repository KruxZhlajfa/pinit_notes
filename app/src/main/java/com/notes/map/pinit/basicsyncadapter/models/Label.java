package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 *
 * This class represents a single label in the JSON feed.
 *
 * <p>It includes all the data members
 */
public class Label extends ModelWithUuid {

    @Expose
    public String name;
    @Expose
    public long date_updated;
    @Expose
    public int deleted;
    @Expose
    public String users_api_key;

    public Label(){
        mUri = FeedContract.Label.CONTENT_URI;
    }

    /**
     *
     * @param name
     * @param users_api_key
     */
   public Label(String name, String users_api_key) {
        this.name = name;
        this.date_updated =  System.currentTimeMillis();
        this.users_api_key = users_api_key;
        this.deleted = 0;
    }

    /**
     *
     * @param name
     * @param users_api_key
     * @param deleted
     */
    public Label(String name, String users_api_key, int deleted) {
        this(name, users_api_key);
        this.deleted = deleted;
    }

    /**
     * Full constructor
     * @param name
     * @param users_api_key
     * @param date_updated
     * @param deleted
     */
    public Label(String name, String users_api_key, long date_updated, int deleted) {
        this(name, users_api_key, deleted);
        this.date_updated = date_updated;
    }


    /**
     * Full constructor
     * @param name
     * @param users_api_key
     * @param date_updated
     * @param deleted
     */
    public Label(String uuid, String name, String users_api_key, long date_updated, int deleted) {
        this(name, users_api_key, deleted, deleted);
        this.uuid = uuid;
        this.date_updated = date_updated;
    }

    @Override
    public List<ContentProviderOperation> save(Context context) {
        super.save(context);
        this.date_updated = System.currentTimeMillis();
        ContentValues values = new ContentValues();
        values.put(FeedContract.Label.COLUMN_NAME_NAME, this.name);
        values.put(FeedContract.Label.COLUMN_NAME_UPDATED, this.date_updated);
        values.put(FeedContract.Label.COLUMN_NAME_DELETED, this.deleted);
        values.put(FeedContract.Label.COLUMN_NAME_USERS_API_KEY, this.users_api_key);
        if (this._id != 0){
            //update
            Uri uri = mUri.buildUpon().appendPath(uuid).build();
                //ako je jos uvijek lokalni note ostavi created
            if (this.synced != FeedContract.SyncableItem.SYNC_STATUS_CREATED)
                values.put(FeedContract.Label.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_UPDATED);
            if (deleted == FeedContract.Label.DELETED)
                this.synced = FeedContract.SyncableItem.SYNC_STATUS_DELETED;
            String[] selection = {uuid};
            mContentResolver.update(uri, values, FeedContract.Label.COLUMN_NAME_UUID + "=?", selection);
        } else {
            //insert new
            values.put(FeedContract.Label.COLUMN_NAME_UUID, this.uuid);
            values.put(FeedContract.Label.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_CREATED);
            Uri uri = mContentResolver.insert(FeedContract.Label.CONTENT_URI, values);
            this._id = ContentUris.parseId(uri);
        }
        return batch;
    }

    @Override
    public void delete() {
        this.deleted = FeedContract.SyncableItem.DELETED;
    }

    public static Label fromCursor(Cursor cursor){
        Label label = null;
        try {
            label = new Label();

            label._id = cursor.getInt(cursor.getColumnIndex(FeedContract.Label.COLUMN_NAME_ID));
            label.uuid = cursor.getString(cursor.getColumnIndex(FeedContract.Label.COLUMN_NAME_UUID));
            label.name = cursor.getString(cursor.getColumnIndex(FeedContract.Label.COLUMN_NAME_NAME));
            label.date_updated = cursor.getLong(cursor.getColumnIndex(FeedContract.Label.COLUMN_NAME_UPDATED));
            label.deleted = cursor.getInt(cursor.getColumnIndex(FeedContract.Label.COLUMN_NAME_DELETED));
            label.users_api_key = cursor.getString(cursor.getColumnIndex(FeedContract.Label.COLUMN_NAME_USERS_API_KEY));
            label.synced = cursor.getInt(cursor.getColumnIndex(FeedContract.SyncableItem.COLUMN_NAME_SYNCED));

        } catch (Exception e) {
            //todo log error
            e.printStackTrace();
        }
        return label;
    }
}
