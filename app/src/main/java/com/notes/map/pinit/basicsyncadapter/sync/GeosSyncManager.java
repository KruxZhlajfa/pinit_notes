package com.notes.map.pinit.basicsyncadapter.sync;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.collections.GeosCollection;
import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;


/**
 * Created by Antonio on 24.1.2016..
 */
public class GeosSyncManager extends AbstractSyncManager {

    /**
     * Projection used when querying content provider. Returns all known fields.
     */
    public static final String[] PROJECTION = new String[]{
            FeedContract.Geo._ID,
            FeedContract.Geo.COLUMN_NAME_ADDRESS,
            FeedContract.Geo.COLUMN_NAME_LONGITUDE,
            FeedContract.Geo.COLUMN_NAME_LATITUDE,
            FeedContract.Geo.COLUMN_NAME_RANGE,
            FeedContract.Geo.COLUMN_NAME_UPDATED,
            FeedContract.Geo.COLUMN_NAME_UUID,
            FeedContract.Geo.COLUMN_NAME_DELETED,
            FeedContract.Geo.COLUMN_NAME_SYNCED
    };

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
    public static final int COLUMN_ADDRESS = 1;
    public static final int COLUMN_LONGITUDE = 2;
    public static final int COLUMN_LATITUDE = 3;
    public static final int COLUMN_RANGE = 4;
    public static final int COLUMN_UPDATED = 5;
    public static final int COLUMN_UUID = 6;
    public static final int COLUMN_DELETED = 7;
    public static final int COLUMN_SYNCED = 8;

    private GeosCollection newGeos;
    private GeosCollection updatedGeos;
    private List<String> deletedGeos;
    private Response<GeosCollection> response;


    public GeosSyncManager(final Context context, Retrofit client, PinitApiInterface service, List<ContentProviderOperation> batch) {
        super(context, client, service, batch);
        newGeos = new GeosCollection();
        updatedGeos = new GeosCollection();
        deletedGeos = new ArrayList<>();
    }

    public void syncNewEntries(final SyncResult syncResult) throws IOException {
        //todo pull date of last sync from prefs
        Call<GeosCollection> call = service.getGeos(
                SessionManager.getInstance(mContext).getSessionApiKey(),
                SessionManager.getInstance(mContext).getSyncTimestamp(FeedContract.Geo.TABLE_NAME));

        response = call.execute();

        if (!response.isSuccess()){
            //todo think of a proper way to handle all status codes
            throw new IOException("Network request error code" + response.code());
        }

        if (response.body() == null)
            return;

        HashMap<String, Geo> entryMap = new HashMap<String, Geo>();
        for (Geo e : response.body().geos) {
            entryMap.put(e.uuid, e);
        }

        // Get list of all items
        Log.i(this.getClass().toString(), "Fetching local entries for merge");
        Uri uri = FeedContract.Geo.CONTENT_URI; // Get all entries

        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        if ( c != null ) {
            Log.i(this.getClass().toString(), "Found " + c.getCount() + " local entries. Computing merge solution...");

            // Find stale data
            int id;
            String uuid;
            String address;
            float longitude;
            float latitude;
            int range;
            long updated;
            int deleted;
            int synced;

            while (c.moveToNext()) {
                syncResult.stats.numEntries++;
                // fetch all data
                id = c.getInt(COLUMN_ID);
                uuid = c.getString(COLUMN_UUID);
                address = c.getString(COLUMN_ADDRESS);
                longitude = c.getFloat(COLUMN_LONGITUDE);
                latitude = c.getFloat(COLUMN_LATITUDE);
                updated = c.getLong(COLUMN_UPDATED);
                range = c.getInt(COLUMN_RANGE);
                deleted = c.getInt(COLUMN_DELETED);
                synced = c.getInt(COLUMN_SYNCED);
                Geo match = entryMap.get(uuid);
                if (match != null) {
                    // Entry exists. Update it and remove from Map to prevent duplicate inserts.
                    entryMap.remove(uuid);

                    Uri existingUri = buildUri(uuid);
                    //ignore an already stale entry
                    if (match.date_updated > updated && match.deleted != FeedContract.SyncableItem.DELETED && deleted != FeedContract.SyncableItem.DELETED) {
                        // Update existing record
                        // todo, removed optimiziation if no change is detected, see if we need it
                        Log.i(this.getClass().toString(), "Scheduling update: " + existingUri);
                        batch.add(ContentProviderOperation.newUpdate(existingUri)
                                .withValue(FeedContract.Geo.COLUMN_NAME_ADDRESS, match.address)
                                .withValue(FeedContract.Geo.COLUMN_NAME_LONGITUDE, match.longitude)
                                .withValue(FeedContract.Geo.COLUMN_NAME_LATITUDE, match.latitude)
                                .withValue(FeedContract.Geo.COLUMN_NAME_UPDATED, match.date_updated)
                                .withValue(FeedContract.Geo.COLUMN_NAME_RANGE, match.range)
                                .withValue(FeedContract.Geo.COLUMN_NAME_SYNCED, FeedContract.Geo.SYNC_STATUS_SYNCED)
                                .withValue(FeedContract.Geo.COLUMN_NAME_DELETED, match.deleted)
                                .build());
                        syncResult.stats.numUpdates++;
                    } else if (match.deleted == FeedContract.SyncableItem.DELETED){
                        // server deleted, delete it locally
                        batch.add(ContentProviderOperation.newDelete(existingUri).build());
                        syncResult.stats.numDeletes++;
                    } else if (updated > match.date_updated && deleted != FeedContract.SyncableItem.DELETED){
                        //local is newer, update server
                        updatedGeos.add(new Geo(id, uuid, address, longitude, latitude, range, updated, deleted));
                    } else if (deleted == FeedContract.SyncableItem.DELETED){
                        // deleted locally, update server
                        deletedGeos.add(uuid);
                    }

                    // non matching, but "dirty" with CREATED flag set, add to the newCollection
                } else if (synced == FeedContract.Geo.SYNC_STATUS_CREATED) {
                    newGeos.add(new Geo(id, uuid, address, longitude, latitude, range, updated, deleted));
                    // non matching, but "dirty" with DIRTY flag set, add to the updatedCollection
                } else if (synced == FeedContract.Geo.SYNC_STATUS_UPDATED) {
                    updatedGeos.add(new Geo(id, uuid, address, longitude, latitude, range, updated, deleted));
                    // non matching, but "dirty" with DELETED flag set, add to the deletedCollection
                } else if (synced == FeedContract.Geo.SYNC_STATUS_DELETED) {
                    deletedGeos.add(uuid);
                }
            }
            c.close();
        }
        // Add fetched non existing entries to database
        for (Geo geo : entryMap.values()) {
            Log.i(this.getClass().toString(), "Scheduling insert: entry_id=" + geo.uuid);
            batch.add(ContentProviderOperation.newInsert(FeedContract.Geo.CONTENT_URI)
                    .withValue(FeedContract.Geo.COLUMN_NAME_ADDRESS, geo.address)
                    .withValue(FeedContract.Geo.COLUMN_NAME_LONGITUDE, geo.longitude)
                    .withValue(FeedContract.Geo.COLUMN_NAME_LATITUDE, geo.latitude)
                    .withValue(FeedContract.Geo.COLUMN_NAME_UPDATED, geo.date_updated)
                    .withValue(FeedContract.Geo.COLUMN_NAME_RANGE, geo.range)
                    .withValue(FeedContract.Geo.COLUMN_NAME_UUID, geo.uuid)
                    .withValue(FeedContract.Geo.COLUMN_NAME_SYNCED, FeedContract.Geo.SYNC_STATUS_SYNCED)
                    .build());
            syncResult.stats.numInserts++;
        }

    }

    public void syncCreatedEntries(final SyncResult syncResult) throws IOException {
        // Let's POST all the new "dirty" entries (that have status = CREATED)
        if (newGeos.size() > 0) {
            Call<GeosCollection> createdCall = service.createGeos(SessionManager.getInstance(mContext).getSessionApiKey(), newGeos);
            response = createdCall.execute();
            if (response.isSuccess()) {
                for (Geo geo : response.body().geos) {
                    Uri existingUri = buildUri(geo.uuid);
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Geo.COLUMN_NAME_SYNCED, FeedContract.Geo.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
        }
    }

    public void syncUpdatedEntries(final SyncResult syncResult) throws IOException {
        // Let's PUT all the updated entries
        // todo think of a correct way to solve (very unlikely) conflicts
        // todo try refactoring into a generic method using metaprogramming, to stay DRY
        if (updatedGeos.size() > 0) {
            Call<GeosCollection> updatedCall = service.updateGeos(SessionManager.getInstance(mContext).getSessionApiKey(), updatedGeos);
            response = updatedCall.execute();
            if (response.isSuccess()) {
                for (Geo note : response.body().geos) {
                    Uri existingUri = FeedContract.Geo.CONTENT_URI.buildUpon()
                            .appendPath(note.uuid).build();
                    // we put tne note status to normal and mark it synced
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Geo.COLUMN_NAME_SYNCED, FeedContract.Geo.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
        }
    }

    public void syncDeletedEntries(final SyncResult syncResult) throws IOException {

        // Let's DELETE all the deleted entries
        if (deletedGeos.size() > 0) {
            for(String uuid : deletedGeos) {
                Call<Geo> deletedCall = service.deleteGeo(SessionManager.getInstance(mContext).getSessionApiKey(), uuid);
                Response<Geo> response = deletedCall.execute();
                if (response.isSuccess()) {
                    Uri existingUri = buildUri(uuid);
                    batch.add(ContentProviderOperation.newDelete(existingUri)
                            .build());
                }
            }
        }

    }

    @Override
    protected Uri buildUri(String id) {
        return FeedContract.Geo.CONTENT_URI.buildUpon().appendPath(id).build();
    }
}
