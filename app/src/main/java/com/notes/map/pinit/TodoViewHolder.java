package com.notes.map.pinit;

import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by iamkr on 26.1.2016..
 */
class TodoViewHolder extends RecyclerViewHolder {

    public TodoViewHolder(View itemView) {
        super(itemView);
        tv1 = (TextView) itemView.findViewById(R.id.card_todo_notes_title);
        tvTodo1 = (TextView) itemView.findViewById(R.id.tv_todo1);
        tvTodo2 = (TextView) itemView.findViewById(R.id.tv_todo2);
        tvTodo3 = (TextView) itemView.findViewById(R.id.tv_todo3);
        tvMoreTodos = (TextView) itemView.findViewById(R.id.tv_more_todos);
        todoLayout1 = (LinearLayout) itemView.findViewById(R.id.card_todo1_linear_layout);
        todoLayout2 = (LinearLayout) itemView.findViewById(R.id.card_todo2_linear_layout);
        todoLayout3 = (LinearLayout) itemView.findViewById(R.id.card_todo3_linear_layout);
        checkBox1 = (CheckBox) itemView.findViewById(R.id.chbx_todo1);
        checkBox2 = (CheckBox) itemView.findViewById(R.id.chbx_todo2);
        checkBox3 = (CheckBox) itemView.findViewById(R.id.chbx_todo3);
        mTodoCard = (LinearLayout) itemView.findViewById(R.id.todo_card_layout);
    }
}
