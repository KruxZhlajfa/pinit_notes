package com.notes.map.pinit;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.SyncAdapter;
import com.notes.map.pinit.basicsyncadapter.models.User;
import com.notes.map.pinit.services.SessionManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit2.Call;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by iamkr on 08/02/2016.
 */
public class EditProfileActivity extends AppCompatActivity implements View.OnClickListener {

    private SessionManager session;
    private User mUser;
    private Bitmap mProfileImageBitmap;
    private Response<User> response;
    private int mServerCallAction;
    private static final int PICK_IMAGE = 1, CROP_IMAGE = 2;
    private static final int UPDATE_USER = 3, DELETE_USER = 4, NO_ACTION = 0;

    @InjectView(R.id.edit_profile_name)EditText mNameText;
    @InjectView(R.id.edit_profile_surname) EditText mSurnameText;
    @InjectView(R.id.edit_profile_email) EditText mEmailText;
    @InjectView(R.id.toolbar_profile) Toolbar mToolbar;
    @InjectView(R.id.btn_save_profile) ImageButton mSaveButton;
    @InjectView(R.id.image_profile) ImageView mProfileImage;
    @InjectView(R.id.btn_choose_image) Button mChooseImageButton;
    @InjectView(R.id.btn_delete_account) Button mDeleteAccountButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Session class instance
        session = SessionManager.getInstance(getApplicationContext());
        session.checkLogin();
        mUser = session.getSessionUserData();

        setContentView(R.layout.activity_profile);
        ButterKnife.inject(this);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Edit profile");

        //fill user data to elements
        if (!TextUtils.isEmpty(mUser.picture)){
            byte[] b = Base64.decode(mUser.picture, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
            mProfileImage.setImageBitmap(bitmap);
        }
        mNameText.setText(mUser.name);
        mSurnameText.setText(mUser.surname);
        mEmailText.setText(mUser.email);
        mProfileImageBitmap = null;
        mServerCallAction = 0;

        mSaveButton.setOnClickListener(this);
        mChooseImageButton.setOnClickListener(this);
        mDeleteAccountButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_choose_image:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
                break;
            case R.id.btn_delete_account:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Delete user " + mUser.username + "?");

                final TextView input = new TextView(this);
                input.setText("Are you sure you want to delete your account? " +
                        "This way you can't use the app again with this account!");
                builder.setView(input);

                builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mServerCallAction = DELETE_USER;
                        AsyncCaller sync = new AsyncCaller();
                        sync.execute();
                    }
                });
                builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                builder.show();
                break;
            case R.id.btn_save_profile:
                if (validate()){
                    mUser.name = mNameText.getText().toString();
                    mUser.surname = mSurnameText.getText().toString();
                    mUser.email = mEmailText.getText().toString();
                    if (mProfileImageBitmap != null) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        mProfileImageBitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] b = baos.toByteArray();
                        String temp = Base64.encodeToString(b, Base64.DEFAULT);
                        mUser.picture = temp;
                    }
                    mServerCallAction = UPDATE_USER;
                    AsyncCaller sync = new AsyncCaller();
                    sync.execute();
                }
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            String realPath;
            if (Build.VERSION.SDK_INT < 11) // SDK < API11
                realPath = RealPathUtil.getRealPathFromURI_BelowAPI11(this, data.getData());
            else if (Build.VERSION.SDK_INT < 19) // SDK >= 11 && SDK < 19
                realPath = RealPathUtil.getRealPathFromURI_API11to18(this, data.getData());
            else // SDK > 19 (Android 4.4)
                realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

            performCrop(realPath);
        }

        if (requestCode == CROP_IMAGE && resultCode == RESULT_OK){
            Uri uri = data.getData();
            try {
                mProfileImageBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                mProfileImage.setImageBitmap(mProfileImageBitmap);
                mProfileImage.setScaleType(ImageView.ScaleType.FIT_XY);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity
            Log.d("CROPIMAGE", "pick uri u cropu " + picUri);

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 280);
            cropIntent.putExtra("outputY", 280);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, CROP_IMAGE);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "Your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private boolean validate(){
        boolean valid = true;
        String email = mEmailText.getText().toString();
        String name = mNameText.getText().toString();
        String surname = mSurnameText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            mNameText.setError("Name can't be empty");
            valid = false;
        } else {
            mNameText.setError(null);
        }

        if (surname.isEmpty() || surname.length() < 3) {
            mSurnameText.setError("Surname can't be empty");
            valid = false;
        } else {
            mSurnameText.setError(null);
        }
        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailText.setError("Enter a valid email address");
            valid = false;
        } else {
            mEmailText.setError(null);
        }
        return valid;
    }

    private void onSaveSuccess(){
        if (mServerCallAction == UPDATE_USER){
            session.updateSessionUserData(response.body());
            Toast.makeText(getBaseContext(), "Saved data to server!", Toast.LENGTH_LONG).show();
        } else if (mServerCallAction == DELETE_USER){
            session.logoutUser();
            Toast.makeText(getBaseContext(), "Account deleted! Sad to see you go :(", Toast.LENGTH_LONG).show();
        }
        mServerCallAction = NO_ACTION;
        finish();
    }

    private void onSaveFailed(){
        if (mServerCallAction == UPDATE_USER)
            Toast.makeText(getBaseContext(), "Failed to save data, check your internet connection!", Toast.LENGTH_SHORT).show();
        else if (mServerCallAction == DELETE_USER)
            Toast.makeText(getBaseContext(), "Failed to delete account, check your internet connection!", Toast.LENGTH_SHORT).show();
        mServerCallAction = NO_ACTION;
    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean>
    {

        final ProgressDialog progressDialog = new ProgressDialog(EditProfileActivity.this,
                R.style.AppTheme_Dark_Dialog);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Saving data to server...");
            progressDialog.show();
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Gson gson = new GsonBuilder().serializeNulls().create();
                //initialize client
                Retrofit client = new Retrofit.Builder()
                        .baseUrl(SyncAdapter.FEED_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                PinitApiInterface service = client.create(PinitApiInterface.class);

                Call<User> call;

                if (mServerCallAction == UPDATE_USER)
                    call = service.updateUser(mUser.email, mUser.api_key, mUser);
                else if (mServerCallAction == DELETE_USER)
                    call = service.deleteUser(mUser.email, mUser.api_key);
                else
                    call = null;

                response = call.execute();

                Log.d("RESPONSE", "Response code:" + response.code());

                return response.isSuccess();
            } catch (Exception e) {
                Log.e("ASYNC EXCEPTION", e.getMessage());
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressDialog.dismiss();
            if (aBoolean) {
                onSaveSuccess();
            } else {
                onSaveFailed();
            }
        }
    }
}
