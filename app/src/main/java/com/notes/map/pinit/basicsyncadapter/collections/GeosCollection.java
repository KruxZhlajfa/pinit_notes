package com.notes.map.pinit.basicsyncadapter.collections;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.models.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 */
public class GeosCollection extends AbstractCollection{
    @Expose
    public List<Geo> geos = new ArrayList<>();

    public void add(Geo geo){
        this.geos.add(geo);
    }

    public int size(){
        if (this.geos== null)
            return 0;
        else
            return this.geos.size();
    }

    @Override
    public Geo get(String id) {
        for (Geo geo : geos){
            if (geo.uuid.equals(id))
                return geo;
        }
        return null;
    }
}
