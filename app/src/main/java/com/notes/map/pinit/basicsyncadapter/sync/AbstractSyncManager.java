package com.notes.map.pinit.basicsyncadapter.sync;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.net.Uri;
import android.util.Log;

import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import retrofit2.Retrofit;


/**
 * Created by Antonio on 23.1.2016..
 */
public abstract class AbstractSyncManager {

    protected final static String ERRORS_TAG = "CLIENTSIDE_SYNC_ERRORS";

    protected final Context mContext;
    protected final Retrofit client;
    protected final PinitApiInterface service;
    protected final ContentResolver contentResolver;
    protected List<ContentProviderOperation> batch;

    public AbstractSyncManager(final Context context, final Retrofit client, final PinitApiInterface service,
                               List<ContentProviderOperation> batch) {
        this.mContext = context;
        this.client = client;
        this.service = service;
        this.contentResolver = context.getContentResolver();
        this.batch = batch;
    }

    public void syncNewEntries(final SyncResult syncResult) throws IOException {

    }

    public void syncCreatedEntries(final SyncResult syncResult) throws IOException {

    }

    public void syncUpdatedEntries(final SyncResult syncResult) throws IOException {

    }

    public void syncDeletedEntries(final SyncResult syncResult) throws IOException {


    }

    protected abstract Uri buildUri(String id);

    protected void logErrors(Map<String, List<String>> errorMap){
        if (errorMap != null) {
            for (Map.Entry<String, List<String>> entry : errorMap.entrySet()) {
                Log.e(ERRORS_TAG, "errors for id:" + entry.getKey());
                for (String error : entry.getValue()) {
                    Log.e(ERRORS_TAG, error);
                }
            }
        }
    }

}
