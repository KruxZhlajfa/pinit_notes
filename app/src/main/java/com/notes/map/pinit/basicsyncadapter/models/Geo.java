package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 *
 * This class represents a single geolocation coordinate in the JSON feed.
 *
 * <p>It includes all the data members
 */
public class Geo extends ModelWithUuid {

    protected Uri mUri = FeedContract.Geo.CONTENT_URI;

    @Expose
    public String address;
    @Expose
    public double longitude;
    @Expose
    public double latitude;
    @Expose
    public int range;
    @Expose
    public long date_updated;
    @Expose
    public int deleted;

    /**
     * Used for creating new Geos
     * @param address
     * @param longitude
     * @param latitude
     * @param range
     */
    public Geo(String address, double longitude, double latitude, int range) {
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.range = range;
        this.date_updated = System.currentTimeMillis();
    }

    /**
     * Full constructor
     * @param uuid
     * @param address
     * @param longitude
     * @param latitude
     * @param range
     * @param date_updated
     */
    public Geo(int _id, String uuid, String address, double longitude, double latitude, int range, long date_updated, int deleted) {
        this(address, longitude, latitude, range);
        this._id = _id;
        this.uuid = uuid;
        this.date_updated = date_updated;
        this.deleted = deleted;
    }

    @Override
    public List<ContentProviderOperation> save(Context context) {
        super.save(context);
        this.date_updated = System.currentTimeMillis();
        ContentValues values = new ContentValues();
        values.put(FeedContract.Geo.COLUMN_NAME_ADDRESS, this.address);
        values.put(FeedContract.Geo.COLUMN_NAME_LONGITUDE, this.longitude);
        values.put(FeedContract.Geo.COLUMN_NAME_LATITUDE, this.latitude);
        values.put(FeedContract.Geo.COLUMN_NAME_RANGE, this.range);
        values.put(FeedContract.Geo.COLUMN_NAME_UPDATED, this.date_updated);
        values.put(FeedContract.Geo.COLUMN_NAME_DELETED, this.deleted);
        if (this._id != 0){
            Uri uri = mUri.buildUpon().appendPath(uuid).build();
            //update
            values.put(FeedContract.Geo.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_UPDATED);
            //ako je jos uvijek lokalni ostavi created
            if (this.synced != FeedContract.SyncableItem.SYNC_STATUS_CREATED)
                values.put(FeedContract.Geo.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_UPDATED);
            if (deleted == FeedContract.Geo.DELETED)
                this.synced = FeedContract.SyncableItem.SYNC_STATUS_DELETED;
            String[] selection = {uuid};
            mContentResolver.update(uri, values, FeedContract.Geo.COLUMN_NAME_UUID + "=?", selection);
        } else {
            //insert new
            values.put(FeedContract.Geo.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_CREATED);
            values.put(FeedContract.Geo.COLUMN_NAME_UUID, uuid);
            Log.d("UUID", uuid);
            Uri uri = mContentResolver.insert(FeedContract.Geo.CONTENT_URI, values);
            this._id = ContentUris.parseId(uri);
        }
        return batch;
    }

    @Override
    public void delete() {
        this.deleted = 1;
    }

    public static Geo fromCursor(Cursor cursor){
        return new Geo(
                cursor.getInt(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_UUID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_ADDRESS)),
                cursor.getDouble(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_LONGITUDE)),
                cursor.getDouble(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_LATITUDE)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_RANGE)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_UPDATED)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Geo.COLUMN_NAME_DELETED)));
    }

}
