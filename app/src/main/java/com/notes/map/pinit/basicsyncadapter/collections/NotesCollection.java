package com.notes.map.pinit.basicsyncadapter.collections;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.basicsyncadapter.models.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 *
 */
public class NotesCollection extends AbstractCollection {
    @Expose
    public List<Note> notes = new ArrayList<>();

    @Expose(serialize = false)
    public List<Note> conflicts;

    public void add(Note note){
        this.notes.add(note);
    }

    public int size(){
        if (this.notes == null)
            return 0;
        else
            return this.notes.size();
    }

    @Override
    public Note get(String id) {
        for (Note note : notes){
            if (note.uuid.equals(id))
                return note;
        }
        return null;
    }
}