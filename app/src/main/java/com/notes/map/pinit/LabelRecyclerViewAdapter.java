package com.notes.map.pinit;

import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.common.CursorRecyclerViewAdapter;

import java.util.ArrayList;

/**
 * Created by Nikolina on 07/02/2016.
 */
public class LabelRecyclerViewAdapter extends CursorRecyclerViewAdapter<LabelRecyclerViewAdapter.ViewHolder> {

    public LabelRecyclerViewAdapter (Context context, Cursor cursor){
        super(context, cursor);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView mTextView;
        public ViewHolder(View viewItem) {
            super(viewItem);
            mTextView = (TextView) itemView.findViewById(R.id.tv_label);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.labels_list, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Cursor cursor) {
        Label label = Label.fromCursor(cursor);
        if (label.deleted == 0)
            viewHolder.mTextView.setText(label.name);
    }



}
