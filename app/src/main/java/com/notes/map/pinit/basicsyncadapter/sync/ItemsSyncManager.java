package com.notes.map.pinit.basicsyncadapter.sync;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.collections.ItemsCollection;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.SessionManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Antonio on 24.1.2016..
 */
public class ItemsSyncManager extends AbstractSyncManager {

    /**
     * Projection used when querying content provider. Returns all known fields.
     */
    public static final String[] PROJECTION = new String[]{
            FeedContract.Item._ID,
            FeedContract.Item.COLUMN_NAME_TEXT,
            FeedContract.Item.COLUMN_NAME_CHECKED,
            FeedContract.Item.COLUMN_NAME_UPDATED,
            FeedContract.Item.COLUMN_NAME_UUID,
            FeedContract.Item.COLUMN_NAME_DELETED,
            FeedContract.Item.COLUMN_NAME_NOTES_ID,
            FeedContract.Item.COLUMN_NAME_SYNCED
    };

    // Constants representing column positions from PROJECTION.
    public static final int COLUMN_ID = 0;
    public static final int COLUMN_TEXT = 1;
    public static final int COLUMN_CHECKED = 2;
    public static final int COLUMN_UPDATED = 3;
    public static final int COLUMN_UUID = 4;
    public static final int COLUMN_DELETED = 5;
    public static final int COLUMN_NOTES_ID = 6;
    public static final int COLUMN_SYNCED = 7;

    private ItemsCollection newItems;
    private ItemsCollection updatedItems;
    private List<String> deletedItems;
    private Response<ItemsCollection> response;

    public ItemsSyncManager(final Context context, Retrofit client, PinitApiInterface service, List<ContentProviderOperation> batch) {
        super(context, client, service, batch);
        newItems = new ItemsCollection();
        updatedItems = new ItemsCollection();
        deletedItems = new ArrayList<>();
    }

    public void syncNewEntries(final SyncResult syncResult) throws IOException {
        //todo read and send last sync time
        Call<ItemsCollection> call = service.getItems(
                SessionManager.getInstance(mContext).getSessionApiKey(),
                SessionManager.getInstance(mContext).getSyncTimestamp(FeedContract.Item.TABLE_NAME));

        response = call.execute();

        if (response.body() == null) {
            Log.e(getClass().getSimpleName(), "Error syncing New Entries, body was null");
            return;
        }

        if (!response.isSuccess()){
            //todo think of a proper way to handle all status codes
            throw new IOException("Network request error code" + response.code());
        }

        HashMap<String, Item> entryMap = new HashMap<>();
        for (Item e : response.body().items) {
            entryMap.put(e.uuid, e);
        }

        // Get list of all items
        Log.i(this.getClass().toString(), "Fetching local entries for merge");
        Uri uri = FeedContract.Item.CONTENT_URI; // Get all entries

        Cursor c = contentResolver.query(uri, PROJECTION, null, null, null);
        if ( c != null ) {
            Log.i(this.getClass().toString(), "Found " + c.getCount() + " local entries. Computing merge solution...");

            // Find stale data
            String uuid;
            String text;
            int checked;
            long updated;
            int deleted;
            String notes_id;
            int synced;

            while (c.moveToNext()) {
                syncResult.stats.numEntries++;
                // fetch all data
                uuid = c.getString(COLUMN_UUID);
                text = c.getString(COLUMN_TEXT);
                checked = c.getInt(COLUMN_CHECKED);
                updated = c.getLong(COLUMN_UPDATED);
                deleted = c.getInt(COLUMN_DELETED);
                notes_id = c.getString(COLUMN_NOTES_ID);
                synced = c.getInt(COLUMN_SYNCED);
                Item match = entryMap.get(uuid);
                if (match != null) {
                    // Entry exists. Update it and remove from Map to prevent duplicate inserts.
                    entryMap.remove(uuid);

                    Uri existingUri = buildUri(uuid);
                    //matching and server is newer ignore an already stale entry from server
                    if (match.date_updated > updated && match.deleted != FeedContract.SyncableItem.DELETED && deleted != FeedContract.SyncableItem.DELETED) {
                        // Update existing record
                        // todo, removed optimiziation if no change is detected, see if we need it
                        Log.i(this.getClass().toString(), "Scheduling update: " + existingUri);
                        updateItem(existingUri, match);
                        syncResult.stats.numUpdates++;
                    } else if (match.deleted == FeedContract.SyncableItem.DELETED){
                        // server deleted the item, delete it locally
                        batch.add(ContentProviderOperation.newDelete(existingUri).build());
                        syncResult.stats.numDeletes++;
                    } else if(updated > match.date_updated && deleted != FeedContract.SyncableItem.DELETED) { //matching but updated local is newer
                        // local is newer, update on server
                        updatedItems.add(new Item(uuid, text, checked, updated, notes_id));
                    } else if (deleted == FeedContract.SyncableItem.DELETED){
                        // deleted locall, server not up to date, add to deletes
                        deletedItems.add(uuid);
                    }

                    // non matching, but "dirty" with CREATED flag set, add to the newCollection
                } else if (synced == FeedContract.Item.SYNC_STATUS_CREATED) {
                    newItems.add(new Item(uuid, text, checked, updated, notes_id));
                    // non matching, but "dirty" with DIRTY flag set, add to the updatedCollection
                } else if (synced == FeedContract.Item.SYNC_STATUS_UPDATED) {
                    updatedItems.add(new Item(uuid, text, checked, updated, notes_id));
                    // non matching, but "dirty" with DELETED flag set, add to the deletedCollection
                } else if (synced == FeedContract.Item.SYNC_STATUS_DELETED) {
                    deletedItems.add(uuid);
                }
            }
            c.close();
        }
        // Add fetched non existing entries to database
        for (Item item : entryMap.values()) {
            Log.i(this.getClass().toString(), "Scheduling insert: entry_id=" + item.uuid);
            batch.add(ContentProviderOperation.newInsert(FeedContract.Item.CONTENT_URI)
                    .withValue(FeedContract.Item.COLUMN_NAME_TEXT, item.text)
                    .withValue(FeedContract.Item.COLUMN_NAME_CHECKED, item.checked)
                    .withValue(FeedContract.Item.COLUMN_NAME_UPDATED, item.date_updated)
                    .withValue(FeedContract.Item.COLUMN_NAME_NOTES_ID, item.notes_id)
                    .withValue(FeedContract.Item.COLUMN_NAME_TEXT, item.text)
                    .withValue(FeedContract.Item.COLUMN_NAME_UUID, item.uuid)
                    .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, FeedContract.Item.SYNC_STATUS_SYNCED)
                    .withValue(FeedContract.Item.COLUMN_NAME_DELETED, item.deleted)
                    .build());
            syncResult.stats.numInserts++;
        }

    }

    public void syncCreatedEntries(final SyncResult syncResult) throws IOException {
        // Let's POST all the new "dirty" entries (that have sync = CREATED)
        if (newItems.size() > 0) {
            Call<ItemsCollection> createdCall = service.createItems(SessionManager.getInstance(mContext).getSessionApiKey(), newItems);
            response = createdCall.execute();

            if(response.body() == null){
                Log.e(getClass().getSimpleName(), "Sync of Created Items failed, body was null");
                return;
            }

            logErrors(response.body().errors);

            //conflict detected, resolve by saving the recieved conflicted Items to local
            for (Item item : (List<Item>)response.body().conflicts) {
                Uri existingUri = buildUri(item.uuid);
                Item oldItem = newItems.get(item.uuid);
                if (oldItem.date_updated < item.date_updated){
                    item._id = oldItem._id;
                    updateItem(existingUri, item);
                } else {
                    // we put tne note status to normal and mark it synced
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, FeedContract.Item.SYNC_STATUS_SYNCED)
                            .build());
                }
            }

            for (Item item : response.body().items) {
                Uri existingUri = buildUri(item.uuid);
                // we put tne note status to normal and mark it synced
                batch.add(ContentProviderOperation.newUpdate(existingUri)
                        .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, FeedContract.Item.SYNC_STATUS_SYNCED)
                        .build());
            }
        }
    }

    public void syncUpdatedEntries(final SyncResult syncResult) throws IOException {
        // Let's PUT all the updated entries
        // todo think of a correct way to solve (very unlikely) conflicts
        // todo try refactoring into a generic method using metaprogramming, to stay DRY
        if (updatedItems.size() > 0) {
            Call<ItemsCollection> updatedCall = service.updateItems(SessionManager.getInstance(mContext).getSessionApiKey(), updatedItems);
            response = updatedCall.execute();
            if(response.isSuccess()) {
                for (Item item : response.body().items) {
                    Uri existingUri = buildUri(item.uuid);
                    // we put tne note status to normal and mark it synced
                    batch.add(ContentProviderOperation.newUpdate(existingUri)
                            .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, FeedContract.Item.SYNC_STATUS_SYNCED)
                            .build());
                }
            }
        }
    }

    public void syncDeletedEntries(final SyncResult syncResult) throws IOException {

        // Let's DELETE all the deleted entries
        if (deletedItems.size() > 0) {
            for(String uuid : deletedItems) {
                Call<Item> deletedCall = service.deleteItem(SessionManager.getInstance(mContext).getSessionApiKey(), uuid);
                Response<Item> response = deletedCall.execute();
                if (response.isSuccess()) {
                    Uri existingUri = buildUri(uuid);
                    batch.add(ContentProviderOperation.newDelete(existingUri)
                            .build());
                }
            }
        }

    }

    @Override
    protected Uri buildUri(String id) {
        return FeedContract.Item.CONTENT_URI.buildUpon().appendPath(id).build();
    }

    private void updateItem(Uri existingUri, Item match){
        batch.add(ContentProviderOperation.newUpdate(existingUri)
                .withValue(FeedContract.Item.COLUMN_NAME_TEXT, match.text)
                .withValue(FeedContract.Item.COLUMN_NAME_CHECKED, match.checked)
                .withValue(FeedContract.Item.COLUMN_NAME_UPDATED, match.date_updated)
                .withValue(FeedContract.Item.COLUMN_NAME_DELETED, match.date_updated)
                .withValue(FeedContract.Item.COLUMN_NAME_NOTES_ID, match.notes_id)
                .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, FeedContract.Item.SYNC_STATUS_SYNCED)
                .withValue(FeedContract.Item.COLUMN_NAME_DELETED, match.deleted)
                .build());
    }

}
