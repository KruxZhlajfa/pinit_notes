package com.notes.map.pinit;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.notes.map.pinit.basicsyncadapter.models.GeoNote;
import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.models.TodoNote;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.common.CursorRecyclerViewAdapter;

/**
 * Created by Antonio on 26.1.2016..
 */
public class NoteRecyclerViewAdapter extends CursorRecyclerViewAdapter<RecyclerViewHolder>{

    private Context mContext;

    public NoteRecyclerViewAdapter(Context context, Cursor cursor) {
        super(context, cursor);
        mContext = context;
    }

    @Override
    public int getItemViewType(int position) {
        if (isDataValid() && getCursor() != null && getCursor().moveToPosition(position)){
            return getCursor().getInt(getCursor().getColumnIndex(FeedContract.Note.COLUMN_NAME_TYPE));
        } else {
            //todo this is not safe, think of a better way to handle errors
            Log.d("DEBUG", "Returned 0, something wrong with Cursor");
            return 0;
        }
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        if (viewType == FeedContract.Note.TYPE_NORMAL){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
            return new NoteViewHolder(v);
        } else if (viewType == FeedContract.Note.TYPE_TODO){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_todo, parent, false);
            return new TodoViewHolder(v);
        } else if (viewType == FeedContract.Note.TYPE_GEO){
            v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_geo, parent, false);
            return new GeoNoteViewHolder(v);
        } else {
            return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, Cursor cursor) { //todo napravi da bude checked ako je checked u bazi
        if (holder.getItemViewType() == FeedContract.Note.TYPE_NORMAL){ //normal note
            Note note = Note.fromCursor(getCursor());
            holder.tv1.setText(note.title);
            holder.tv2.setText(note.text);
            holder.mNoteCard.setBackgroundColor(Color.parseColor(note.color));
        } else if (holder.getItemViewType() == FeedContract.Note.TYPE_GEO){
            GeoNote note = GeoNote.fromCursor(getCursor());
            holder.tvGeo1.setText(note.title);
            holder.tvGeo2.setText(note.text);
            holder.tvGeo3.setText(note.getGeo(mContext).address);
            holder.mGeoNoteCard.setBackgroundColor(Color.parseColor(note.color));
        } else if (holder.getItemViewType() == FeedContract.Note.TYPE_TODO){ //to-do note
            TodoNote note = TodoNote.fromCursor(getCursor());
            holder.tv1.setText(note.title);
            holder.mTodoCard.setBackgroundColor(Color.parseColor(note.color));

            //no todos - hide everything
            holder.todoLayout1.setVisibility(LinearLayout.GONE);
            holder.todoLayout2.setVisibility(LinearLayout.GONE);
            holder.todoLayout3.setVisibility(LinearLayout.GONE);

            int size = (note.getTodos(mContext).size() >= 3 )? 3 : note.getTodos(mContext).size();

            switch (size){
                case(3):
                    holder.todoLayout3.setVisibility(LinearLayout.VISIBLE);
                    holder.tvTodo3.setText(note.getTodos(2).text);
                    holder.checkBox3.setChecked(note.getTodos(2).isChecked());
                    if (note.getTodos(mContext).size() > 3) //todo stavi visible i kad ima točno 3 todoa
                        holder.tvMoreTodos.setVisibility(TextView.VISIBLE);
                case(2):
                    holder.todoLayout2.setVisibility(LinearLayout.VISIBLE);
                    holder.tvTodo2.setText(note.getTodos(1).text);
                    holder.checkBox2.setChecked(note.getTodos(1).isChecked());
                case(1):
                    holder.todoLayout1.setVisibility(LinearLayout.VISIBLE);
                    holder.tvTodo1.setText(note.getTodos(0).text);
                    holder.checkBox1.setChecked(note.getTodos(0).isChecked());
                    break;

            }
        }
    }


}
