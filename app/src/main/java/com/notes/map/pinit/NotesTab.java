package com.notes.map.pinit;

/**
 * Created by iamkr on 9.12.2015..
 */
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.ModelRepository;

public class NotesTab extends Fragment {

    /**
     * Cursor adapter for controlling CardView results.
     */
    private NoteRecyclerViewAdapter mAdapter;

    @Override
    public void onResume() {
        super.onResume();
        mAdapter.registerContentObserver(this.getContext(), FeedContract.Note.CONTENT_URI);
    }

    @Override
    public void onPause() {
        super.onPause();
        mAdapter.unregisterContentObserver(this.getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_notes,container,false);

        RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        ModelRepository repository = ModelRepository.getInstance(getActivity().getApplicationContext());
        final Cursor c = repository.findNotes();
        mAdapter = new NoteRecyclerViewAdapter(getContext(), c);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);

        //Layout manager for Recycler view

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addOnItemTouchListener( // and the click is handled
                new RecyclerClickListener(v.getContext(), new RecyclerClickListener.SimpleOnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), DetailedActivity.class);
                        c.moveToPosition(position);
                        intent.putExtra(DetailedActivity.ID, Note.fromCursor(c));
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                                getActivity(), view, "transition_notes_title");
                        ActivityCompat.startActivity(getActivity(), intent, options.toBundle());
                    }
                }));
        return v;
    }
}
