package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.services.NoteFactory;

import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 *
 * This class represents a single note in the JSON feed.
 *
 * <p>It includes all the data members
 */
public class Note extends ModelWithUuid implements Parcelable {

    @Expose
    public String title;
    @Expose
    public long date_created;
    @Expose
    public long date_updated;
    @Expose
    public long date_alarm;
    @Expose
    public String text;
    @Expose
    public int priority;
    @Expose
    public String color;
    @Expose
    public int status;
    @Expose
    public String labels_id;
    @Expose
    public String geos_id;
    @Expose
    public int type;

    public Note(){
        mUri = FeedContract.Note.CONTENT_URI;
    }

    /**
     * Used for creating a new note
     * @param title
     * @param date_alarm
     * @param text
     * @param priority
     * @param color
     * @param status
     * @param labels_id
     */
    public Note(String title, long date_alarm, String text,
                int priority, String color, int status, String labels_id){
        this();
        this.title = title;
        this.date_created = System.currentTimeMillis();
        this.date_alarm = date_alarm;
        this.text = text;
        this.priority = priority;
        this.color = color;
        this.status = status;
        this.labels_id = labels_id;
    }

    /**
     * Full constructor, without the local ID column
     * @param uuid
     * @param title
     * @param date_created
     * @param date_updated
     * @param date_alarm
     * @param text
     * @param priority
     * @param color
     * @param status
     * @param labels_id
     * @param geos_id
     */
    public Note(String uuid, String title, long date_created, long date_updated, long date_alarm, String text,
         int priority, String color, int status, String labels_id, String geos_id) {
        this(title, date_alarm, text, priority, color, status, labels_id);
        this.date_created = date_created;
        this.uuid = uuid;
        this.date_updated = date_updated;
        this.geos_id = geos_id;
    }

    /**
     * Full constructor, with local ID. Use when pulling results from local database.
     * @param id
     * @param uuid
     * @param title
     * @param date_created
     * @param date_updated
     * @param date_alarm
     * @param text
     * @param priority
     * @param color
     * @param status
     * @param labels_id
     * @param geos_id
     */
    public Note(long id, String uuid, String title, long date_created, long date_updated, long date_alarm, String text,
                int priority, String color, int status, String labels_id, String geos_id, int type) {
        this(uuid, title, date_created, date_updated, date_alarm, text, priority, color, status, labels_id, geos_id);
        this._id = id;
        this.type = type;
    }

    public static final Creator<Note> CREATOR = new Creator<Note>() {
        @Override
        public Note createFromParcel(Parcel in) {
            return new Note(in);
        }

        @Override
        public Note[] newArray(int size) {
            return new Note[size];
        }
    };

    public int getType(){
        return type;
    }

    @Override
    public List<ContentProviderOperation> save(Context context) {
        super.save(context);
        this.date_updated = System.currentTimeMillis();
        if (this.isPersisted()){
            //update
            //let's check for the deleted status first
            if (synced != FeedContract.Note.SYNC_STATUS_CREATED)
                this.synced = FeedContract.SyncableItem.SYNC_STATUS_UPDATED;
            if (status == FeedContract.Note.STATUS_DELETED)
                this.synced = FeedContract.SyncableItem.SYNC_STATUS_DELETED;

            Uri existingUri = mUri.buildUpon().appendPath(uuid).build();
            batch.add(ContentProviderOperation.newUpdate(existingUri)
                    .withValue(FeedContract.Note.COLUMN_NAME_TITLE, this.title)
                    .withValue(FeedContract.Note.COLUMN_NAME_TEXT, this.text)
                    .withValue(FeedContract.Note.COLUMN_NAME_CREATED, this.date_created)
                    .withValue(FeedContract.Note.COLUMN_NAME_UPDATED, this.date_updated)
                    .withValue(FeedContract.Note.COLUMN_NAME_ALARM, this.date_alarm)
                    .withValue(FeedContract.Note.COLUMN_NAME_PRIORITY, this.priority)
                    .withValue(FeedContract.Note.COLUMN_NAME_COLOR, this.color)
                    .withValue(FeedContract.Note.COLUMN_NAME_STATUS, this.status)
                    .withValue(FeedContract.Note.COLUMN_NAME_LABELS_ID, this.labels_id)
                    .withValue(FeedContract.Note.COLUMN_NAME_TYPE, this.type)
                    .withValue(FeedContract.Note.COLUMN_NAME_GEOS_ID, this.geos_id)
                    .withValue(FeedContract.Note.COLUMN_NAME_UUID, this.uuid)
                    .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, this.synced)
                    .build());
        } else {
            //insert new
            batch.add(ContentProviderOperation.newInsert(mUri)
                    .withValue(FeedContract.Note.COLUMN_NAME_TITLE, this.title)
                    .withValue(FeedContract.Note.COLUMN_NAME_TEXT, this.text)
                    .withValue(FeedContract.Note.COLUMN_NAME_CREATED, this.date_created)
                    .withValue(FeedContract.Note.COLUMN_NAME_UPDATED, this.date_updated)
                    .withValue(FeedContract.Note.COLUMN_NAME_ALARM, this.date_alarm)
                    .withValue(FeedContract.Note.COLUMN_NAME_PRIORITY, this.priority)
                    .withValue(FeedContract.Note.COLUMN_NAME_COLOR, this.color)
                    .withValue(FeedContract.Note.COLUMN_NAME_STATUS, this.status)
                    .withValue(FeedContract.Note.COLUMN_NAME_LABELS_ID, this.labels_id)
                    .withValue(FeedContract.Note.COLUMN_NAME_TYPE, this.type)
                    .withValue(FeedContract.Note.COLUMN_NAME_GEOS_ID, this.geos_id)
                    .withValue(FeedContract.Note.COLUMN_NAME_UUID, this.uuid)
                    .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_CREATED)
                    .build());
        }
        return batch;
    }

    @Override
    public void delete() {
        this.status = FeedContract.Note.STATUS_DELETED;
    }

    /**
     * Since Java is a horrendously limiting language claiming to "protect" the
     * programmer we can't have an abstract generic static method that will also behave
     * polymorphically and that's why every subclass of Model has to have it's own separate
     * fromCursor method.
     *
     * Of course this is handy if the Model is not persisted in the database at all, since then
     * there's no need to implement it.
     *
     * Java 8 supposedly supports it through Interfaces, but unfortunately
     * Android is still on Java 6/7.
     *
     * @param cursor
     * @return
     */
    public static Note fromCursor(Cursor cursor){
        int type =  cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TYPE));
        Note note = null;
        try {
            note = NoteFactory.createNote(type);

            note._id = cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ID));
            note.uuid = cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UUID));
            note.title = cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TITLE));
            note.date_created = cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_CREATED));
            note.date_updated = cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UPDATED);
            note.date_alarm = cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ALARM));
            note.text = cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TEXT));
            note.priority = cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_PRIORITY));
            note.color =  cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_COLOR));
            note.status = cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_STATUS));
            note.labels_id = cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_LABELS_ID));
            note.geos_id =  cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_GEOS_ID));
            note.type = cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TYPE));
            note.synced = cursor.getInt(cursor.getColumnIndex(FeedContract.SyncableItem.COLUMN_NAME_SYNCED));
        } catch (Exception e) {
            //todo log error
            e.printStackTrace();
        }
        return note;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mUri.toString());
        dest.writeLong(this._id);
        dest.writeString(this.uuid);
        dest.writeString(this.title);
        dest.writeLong(this.date_created);
        dest.writeLong(this.date_updated);
        dest.writeLong(this.date_alarm);
        dest.writeString(this.text);
        dest.writeInt(this.priority);
        dest.writeString(this.color);
        dest.writeInt(this.status);
        dest.writeString(this.labels_id);
        dest.writeString(this.geos_id);
        dest.writeInt(this.type);
        dest.writeInt(this.synced);
    }

    protected Note(Parcel in) {
        this.mUri =  Uri.parse(in.readString());
        this._id = in.readLong();
        this.uuid = in.readString();
        this.title = in.readString();
        this.date_created = in.readLong();
        this.date_updated = in.readLong();
        this.date_alarm = in.readLong();
        this.text = in.readString();
        this.priority = in.readInt();
        this.color = in.readString();
        this.status = in.readInt();
        this.labels_id = in.readString();
        this.geos_id = in.readString();
        this.type = in.readInt();
        this.synced = in.readInt();
    }

    public void addLabel(Label label) {
        this.labels_id = (label != null)? label.uuid : null;
    }
}