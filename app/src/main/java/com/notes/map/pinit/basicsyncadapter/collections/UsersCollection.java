package com.notes.map.pinit.basicsyncadapter.collections;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 */
public class UsersCollection extends AbstractCollection {

    @Expose(serialize = false)
    public List<User> conflicts;

    @Expose
    public List<User> users = new ArrayList<>();

    public void add(User user){
        this.users.add(user);
    }

    public int size(){
        if (this.users == null)
            return 0;
        else
            return this.users.size();
    }

    @Override
    public User get(String id) {
        for (User user : users){
            if (user.api_key.equals(id))
                return user;
        }
        return null;
    }

}
