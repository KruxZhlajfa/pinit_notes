package com.notes.map.pinit;

import android.content.pm.LabeledIntent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * Created by iamkr on 26.1.2016..
 */
class NoteViewHolder extends RecyclerViewHolder {

    public NoteViewHolder(View itemView) {
        super(itemView);
        tv1 = (TextView) itemView.findViewById(R.id.card_notes_title);
        tv2 = (TextView) itemView.findViewById(R.id.card_notes_desc);
        mNoteCard = (LinearLayout) itemView.findViewById(R.id.note_card_layout);
    }
}
