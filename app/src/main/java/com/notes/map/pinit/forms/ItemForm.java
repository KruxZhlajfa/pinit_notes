package com.notes.map.pinit.forms;

import android.app.Activity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.notes.map.pinit.basicsyncadapter.models.Item;

/**
 * Created by Antonio on 27.1.2016..
 */
public class ItemForm extends ModelForm {

    Activity mContext;
    LinearLayout mParentLayout;
    LinearLayout mTodoLayout;
    CheckBox mCheckBox;
    EditText mEditText;
    Item mItem;

    /**
     *
     * @param context  The Context to use when creating Widgets
     * @param parentLayout The layout of the parent view for this Form
     * @param item The instance of Item Model to bind to this Form
     */
    public ItemForm(Activity context, final LinearLayout parentLayout, final Item item) {
        mItem = item;
        mContext = context;
        mParentLayout = parentLayout;
        mTodoLayout = new LinearLayout(context);

        LinearLayout.LayoutParams linearParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        linearParams.setMargins(convertDpToPx(16), 0, 0, convertDpToPx(16));

        mTodoLayout.setLayoutParams(linearParams);
        mTodoLayout.setOrientation(LinearLayout.HORIZONTAL);

        mCheckBox = new CheckBox(mContext);
        mCheckBox.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mCheckBox.setChecked(mItem.isChecked());
        mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                mItem.setChecked(b);
            }
        });

        mEditText = new EditText(mContext);
        mEditText.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        mEditText.setBackground(null);
        mEditText.setHint("Enter todo");
        mEditText.setText(mItem.text);
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mItem.text = editable.toString();
            }
        });
        mTodoLayout.addView(mCheckBox);
        mTodoLayout.addView(mEditText);

        mParentLayout.addView(mTodoLayout, mParentLayout.getChildCount() - 1);
    }

    public Item getModel(){
        mItem.text = mEditText.getText().toString();
        mItem.checked = mCheckBox.isChecked() ? 1 : 0;
        return mItem;
    }

    public boolean isValid(){
        return !TextUtils.isEmpty(mEditText.getText().toString());
    }

    private int convertDpToPx(int dp){
        return Math.round(dp*(mContext.getResources().getDisplayMetrics().xdpi/ DisplayMetrics.DENSITY_DEFAULT));
    }
}
