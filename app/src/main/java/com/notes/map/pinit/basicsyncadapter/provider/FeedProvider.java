/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.notes.map.pinit.basicsyncadapter.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.util.Log;

import com.notes.map.pinit.common.db.SelectionBuilder;

import java.util.List;


public class FeedProvider extends ContentProvider {
    FeedDatabase mDatabaseHelper;

    /**
     * Content authority for this provider.
     */
    private static final String AUTHORITY = FeedContract.CONTENT_AUTHORITY;

    // The constants below represent individual URI routes, as IDs. Every URI pattern recognized by
    // this ContentProvider is defined using sUriMatcher.addURI(), and associated with one of these
    // IDs.
    //
    // When a incoming URI is run through sUriMatcher, it will be tested against the defined
    // URI patterns, and the corresponding route ID will be returned.
    /*
     *todo ovdje ce se morati definirati id-ovi za sve route
     */
    /**
     * URI ID for route: /notes
     */
    public static final int ROUTE_NOTES = 1;

    /**
     * URI ID for route: /notes/{ID}
     */
    public static final int ROUTE_NOTES_ID = 2;

    /**
     * URI ID for route: /labels
     */
    public static final int ROUTE_LABELS = 3;

    /**
     * URI ID for route: /labels/{ID}
     */
    public static final int ROUTE_LABELS_ID = 4;

    /**
     * URI ID for route: /items
     */
    public static final int ROUTE_ITEMS = 5;

    /**
     * URI ID for route: /items/{ID}
     */
    public static final int ROUTE_ITEMS_ID = 6;

    /**
     * URI ID for route: /geos
     */
    public static final int ROUTE_GEOS = 7;

    /**
     * URI ID for route: /items/{ID}
     */
    public static final int ROUTE_GEOS_ID = 8;

    /**
     * URI ID for route: /users
     */
    public static final int ROUTE_USERS = 9;

    /**
     * URI ID for route: /users/{ID}
     */
    public static final int ROUTE_USERS_ID = 10;

    /**
     * URI ID for route: /users/notes
     */
    public static final int ROUTE_USERS_NOTES = 11;

    /**
     * URI ID for route: /users/notes/{USER_ID}/{NOTE_ID}
     */
    public static final int ROUTE_USERS_NOTES_ID = 12;




    /**
     * UriMatcher, used to decode incoming URIs.
     */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(AUTHORITY, "notes", ROUTE_NOTES);
        sUriMatcher.addURI(AUTHORITY, "notes/*", ROUTE_NOTES_ID);
        sUriMatcher.addURI(AUTHORITY, "labels", ROUTE_LABELS);
        sUriMatcher.addURI(AUTHORITY, "labels/*", ROUTE_LABELS_ID);
        sUriMatcher.addURI(AUTHORITY, "items", ROUTE_ITEMS);
        sUriMatcher.addURI(AUTHORITY, "items/*", ROUTE_ITEMS_ID);
        sUriMatcher.addURI(AUTHORITY, "geos", ROUTE_GEOS);
        sUriMatcher.addURI(AUTHORITY, "geos/*", ROUTE_GEOS_ID);
        sUriMatcher.addURI(AUTHORITY, "users/notes", ROUTE_USERS_NOTES);
        sUriMatcher.addURI(AUTHORITY, "users/notes/*/*", ROUTE_USERS_NOTES_ID);
        sUriMatcher.addURI(AUTHORITY, "users", ROUTE_USERS);
        sUriMatcher.addURI(AUTHORITY, "users/*", ROUTE_USERS_ID);
    }

    @Override
    public boolean onCreate() {
        mDatabaseHelper = new FeedDatabase(getContext());
        return true;
    }

    /**
     * Determine the mime type for notes and other  returned by a given URI.
     */
    /* todo prosirit za druge tablice */
    @Override
    public String getType(Uri uri) {
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_NOTES:
                return FeedContract.Note.CONTENT_TYPE;
            case ROUTE_NOTES_ID:
                return FeedContract.Note.CONTENT_ITEM_TYPE;
            case ROUTE_LABELS:
                return FeedContract.Label.CONTENT_TYPE;
            case ROUTE_LABELS_ID:
                return FeedContract.Label.CONTENT_ITEM_TYPE;
            case ROUTE_ITEMS:
                return FeedContract.Item.CONTENT_TYPE;
            case ROUTE_ITEMS_ID:
                return FeedContract.Item.CONTENT_ITEM_TYPE;
            case ROUTE_USERS:
                return FeedContract.User.CONTENT_TYPE;
            case ROUTE_USERS_ID:
                return FeedContract.User.CONTENT_ITEM_TYPE;
            case ROUTE_GEOS:
                return FeedContract.Geo.CONTENT_TYPE;
            case ROUTE_GEOS_ID:
                return FeedContract.Geo.CONTENT_ITEM_TYPE;
            case ROUTE_USERS_NOTES:
                return FeedContract.UserNote.CONTENT_TYPE;
            case ROUTE_USERS_NOTES_ID:
                return FeedContract.UserNote.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * Perform a database query by URI.
     *
     * <p>Currently supports returning all entries (/entries)
     */
    /*
    todo trebat ce prosiriti ove klase da podrzavaju sve tablice
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        SelectionBuilder builder = new SelectionBuilder();
        int uriMatch = sUriMatcher.match(uri);
        Cursor c;
        Context ctx = getContext();
        switch (uriMatch) {
            case ROUTE_NOTES_ID:
                // Return a single note, by UUID.
                String id = uri.getLastPathSegment();
                builder.where(FeedContract.Note.COLUMN_NAME_UUID + "=?", id);
            case ROUTE_NOTES:
                // Return all known notes
                builder.table(FeedContract.Note.TABLE_NAME)
                       .where(selection, selectionArgs);
                c = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            case ROUTE_LABELS:
                builder.table(FeedContract.Label.TABLE_NAME)
                        .where(selection, selectionArgs);
                c = builder.query(db, projection, sortOrder);
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            case ROUTE_ITEMS:
                builder.table(FeedContract.Item.TABLE_NAME)
                        .where(selection, selectionArgs);
                c = builder.query(db, projection, sortOrder);
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            case ROUTE_GEOS:
                // Return all known notes
                builder.table(FeedContract.Geo.TABLE_NAME)
                        .where(selection, selectionArgs);
                c = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            case ROUTE_USERS:
                // Return all known notes
                builder.table(FeedContract.User.TABLE_NAME)
                        .where(selection, selectionArgs);
                c = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            case ROUTE_USERS_NOTES:
                // Return all known notes
                builder.table(FeedContract.UserNote.TABLE_NAME)
                        .where(selection, selectionArgs);
                c = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }

    /**
     * Insert a new entry into the database.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        assert db != null;
        final int match = sUriMatcher.match(uri);
        Uri result;
        long id;
        switch (match) {
            case ROUTE_NOTES:
                id = db.insertOrThrow(FeedContract.Note.TABLE_NAME, null, values);
                result = Uri.parse(FeedContract.Note.CONTENT_URI + "/" + id);
                break;
            case ROUTE_LABELS:
                id = db.insertOrThrow(FeedContract.Label.TABLE_NAME, null, values);
                result = Uri.parse(FeedContract.Label.CONTENT_URI + "/" + id);
                break;
            case ROUTE_ITEMS:
                id = db.insertOrThrow(FeedContract.Item.TABLE_NAME, null, values);
                result = Uri.parse(FeedContract.Item.CONTENT_URI + "/" + id);
                break;
            case ROUTE_GEOS:
                id = db.insertOrThrow(FeedContract.Geo.TABLE_NAME, null, values);
                result = Uri.parse(FeedContract.Geo.CONTENT_URI + "/" + id);
                break;
            case ROUTE_USERS:
                id = db.insertOrThrow(FeedContract.User.TABLE_NAME, null, values);
                result = Uri.parse(FeedContract.User.CONTENT_URI + "/" + id);
                break;
            case ROUTE_USERS_NOTES:
                id = db.insertOrThrow(FeedContract.UserNote.TABLE_NAME, null, values);
                result = Uri.parse(FeedContract.UserNote.CONTENT_URI + "/" + id);
                break;
            case ROUTE_NOTES_ID:
            case ROUTE_LABELS_ID:
            case ROUTE_GEOS_ID:
            case ROUTE_ITEMS_ID:
            case ROUTE_USERS_NOTES_ID:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return result;
    }

    /**
     * Delete an entry by database by URI.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        String id = uri.getLastPathSegment();
        switch (match) {
            case ROUTE_NOTES_ID:
                count = builder.table(FeedContract.Note.TABLE_NAME)
                        .where(FeedContract.Label.COLUMN_NAME_UUID + "=?", id)
                        .delete(db);
                break;
            case ROUTE_LABELS_ID:
                count = builder.table(FeedContract.Label.TABLE_NAME)
                       .where(FeedContract.Label.COLUMN_NAME_UUID + "=?", id)
                       .where(selection, selectionArgs)
                       .delete(db);
                break;
            case ROUTE_ITEMS_ID:
                count = builder.table(FeedContract.Item.TABLE_NAME)
                        .where(FeedContract.Item.COLUMN_NAME_UUID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            case ROUTE_GEOS_ID:
                count = builder.table(FeedContract.Geo.TABLE_NAME)
                        .where(FeedContract.Geo.COLUMN_NAME_UUID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            case ROUTE_USERS_ID:
                count = builder.table(FeedContract.User.TABLE_NAME)
                        .where(FeedContract.User.COLUMN_NAME_API_KEY + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            case ROUTE_USERS_NOTES_ID:
                List<String> segments = uri.getPathSegments();
                // users_notes URI is /users/notes/<users_api_key>/<note_id>
                String user_id = segments.get(segments.size()-1);
                count = builder.table(FeedContract.UserNote.TABLE_NAME)
                        .where(FeedContract.UserNote.COLUMN_NAME_API_KEY + "=?", user_id)
                        .where(FeedContract.UserNote.COLUMN_NAME_NOTES_UUID + "=?", id)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * Update an entry in the database by URI.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        String id;
        switch (match) {
            case ROUTE_NOTES:
                count = builder.table(FeedContract.Note.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_NOTES_ID:
                id = uri.getLastPathSegment();
                count = builder.table(FeedContract.Note.TABLE_NAME)
                        .where(FeedContract.Note.COLUMN_NAME_UUID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_LABELS_ID:
                id = uri.getLastPathSegment();
                count = builder.table(FeedContract.Label.TABLE_NAME)
                        .where(FeedContract.Label.COLUMN_NAME_UUID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_ITEMS_ID:
                id = uri.getLastPathSegment();
                count = builder.table(FeedContract.Item.TABLE_NAME)
                        .where(FeedContract.Item.COLUMN_NAME_UUID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_GEOS_ID:
                id = uri.getLastPathSegment();
                count = builder.table(FeedContract.Geo.TABLE_NAME)
                        .where(FeedContract.Geo.COLUMN_NAME_UUID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_USERS_ID:
                id = uri.getLastPathSegment();
                count = builder.table(FeedContract.User.TABLE_NAME)
                        .where(FeedContract.User.COLUMN_NAME_API_KEY + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_USERS_NOTES_ID:
                List<String> segments = uri.getPathSegments();
                // users_notes URI is /users/notes/<user_api_key>/<note_id>
                id = segments.get(segments.size() - 2);
                String note_id = segments.get(segments.size()-1);
                count = builder.table(FeedContract.UserNote.TABLE_NAME)
                        .where(FeedContract.UserNote.COLUMN_NAME_NOTES_UUID + "=?", note_id)
                        .where(FeedContract.UserNote.COLUMN_NAME_API_KEY + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * SQLite backend for @{link FeedProvider}.
     *
     * Provides access to an disk-backed, SQLite datastore which is utilized by FeedProvider. This
     * database should never be accessed by other parts of the application directly.
     */
    /*
    todo ovo ce trebat updejtat sa dodatnim konstantama za sve tablice?
     */
    static class FeedDatabase extends SQLiteOpenHelper {
        /** Schema version. */
        public static final int DATABASE_VERSION = 1;
        /** Filename for SQLite file. */
        public static final String DATABASE_NAME = "feed.db";

        private static final String TYPE_TEXT = " TEXT";
        private static final String TYPE_INTEGER = " INTEGER";
        private static final String TYPE_FLOAT = " FLOAT";
        private static final String TYPE_BLOB = " BLOB";
        private static final String COMMA_SEP = ",";
        /** SQL statement to create "notes" table. */
        //todo add foreign keys
        private static final String SQL_CREATE_NOTES =
                "CREATE TABLE " + FeedContract.Note.TABLE_NAME + " (" +
                        FeedContract.Note._ID + " INTEGER PRIMARY KEY," +
                        FeedContract.Note.COLUMN_NAME_TITLE + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_CREATED + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_UPDATED + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_ALARM + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_TEXT + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_PRIORITY + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_COLOR + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_STATUS + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_UUID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_LABELS_ID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_GEOS_ID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_TYPE + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Note.COLUMN_NAME_SYNCED + TYPE_INTEGER + ")";

        private static final String SQL_CREATE_LABELS =
                "CREATE TABLE " + FeedContract.Label.TABLE_NAME + " (" +
                        FeedContract.Label._ID + " INTEGER PRIMARY KEY," +
                        FeedContract.Label.COLUMN_NAME_NAME + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Label.COLUMN_NAME_UPDATED + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Label.COLUMN_NAME_DELETED + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Label.COLUMN_NAME_UUID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Label.COLUMN_NAME_USERS_API_KEY + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Label.COLUMN_NAME_SYNCED + TYPE_INTEGER + ")";

        private static final String SQL_CREATE_ITEMS =
                "CREATE TABLE " + FeedContract.Item.TABLE_NAME + " (" +
                        FeedContract.Item._ID + " INTEGER PRIMARY KEY," +
                        FeedContract.Item.COLUMN_NAME_TEXT + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Item.COLUMN_NAME_CHECKED + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Item.COLUMN_NAME_UPDATED + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Item.COLUMN_NAME_UUID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Item.COLUMN_NAME_DELETED + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Item.COLUMN_NAME_NOTES_ID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Item.COLUMN_NAME_SYNCED + TYPE_INTEGER + ")";

        private static final String SQL_CREATE_GEOS =
                "CREATE TABLE " + FeedContract.Geo.TABLE_NAME + " (" +
                        FeedContract.Geo._ID + " INTEGER PRIMARY KEY," +
                        FeedContract.Geo.COLUMN_NAME_ADDRESS + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Geo.COLUMN_NAME_LONGITUDE + TYPE_FLOAT + COMMA_SEP +
                        FeedContract.Geo.COLUMN_NAME_LATITUDE + TYPE_FLOAT + COMMA_SEP +
                        FeedContract.Geo.COLUMN_NAME_RANGE + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Geo.COLUMN_NAME_UPDATED + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Geo.COLUMN_NAME_UUID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.Geo.COLUMN_NAME_DELETED + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.Geo.COLUMN_NAME_SYNCED + TYPE_INTEGER + ")";

        private static final String SQL_CREATE_USERS =
                "CREATE TABLE " + FeedContract.User.TABLE_NAME + " (" +
                        FeedContract.User._ID + " INTEGER PRIMARY KEY," +
                        FeedContract.User.COLUMN_NAME_EMAIL + TYPE_TEXT + COMMA_SEP +
                        FeedContract.User.COLUMN_NAME_NAME + TYPE_TEXT + COMMA_SEP +
                        FeedContract.User.COLUMN_NAME_SURNAME + TYPE_TEXT + COMMA_SEP +
                        FeedContract.User.COLUMN_NAME_PASSWORD + TYPE_TEXT + COMMA_SEP +
                        FeedContract.User.COLUMN_NAME_PICTURE + TYPE_BLOB + COMMA_SEP +
                        FeedContract.User.COLUMN_NAME_DELETED + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.User.COLUMN_NAME_USERNAME+ TYPE_TEXT + COMMA_SEP +
                        FeedContract.User.COLUMN_NAME_SYNCED + TYPE_INTEGER + ")";

        private static final String SQL_CREATE_USERS_NOTES =
                "CREATE TABLE " + FeedContract.UserNote.TABLE_NAME + " (" +
                        FeedContract.UserNote._ID + " INTEGER PRIMARY KEY," +
                        FeedContract.UserNote.COLUMN_NAME_NOTES_UUID + TYPE_TEXT + COMMA_SEP +
                        FeedContract.UserNote.COLUMN_NAME_API_KEY + TYPE_TEXT + COMMA_SEP +
                        FeedContract.UserNote.COLUMN_NAME_CAN_EDIT + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.UserNote.COLUMN_NAME_CAN_DELETE + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.UserNote.COLUMN_NAME_OWNER + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.UserNote.COLUMN_NAME_UPDATED + TYPE_TEXT + COMMA_SEP +
                        FeedContract.UserNote.COLUMN_NAME_DELETED + TYPE_INTEGER + COMMA_SEP +
                        FeedContract.UserNote.COLUMN_NAME_SYNCED + TYPE_INTEGER + ")";


        /** SQL statement to drop "notes" table. */
        private static final String SQL_DELETE_NOTES =
                "DROP TABLE IF EXISTS " + FeedContract.Note.TABLE_NAME;
        /** SQL statement to drop "labels" table. */
        private static final String SQL_DELETE_LABELS =
                "DROP TABLE IF EXISTS " + FeedContract.Label.TABLE_NAME;
        /** SQL statement to drop "items" table. */
        private static final String SQL_DELETE_ITEMS =
                "DROP TABLE IF EXISTS " + FeedContract.Item.TABLE_NAME;
        /** SQL statement to drop "geos" table. */
        private static final String SQL_DELETE_GEOS =
                "DROP TABLE IF EXISTS " + FeedContract.Geo.TABLE_NAME;
        /** SQL statement to drop "users" table. */
        private static final String SQL_DELETE_USERS =
                "DROP TABLE IF EXISTS " + FeedContract.User.TABLE_NAME;
        /** SQL statement to drop "users_notes" table. */
        private static final String SQL_DELETE_USERS_NOTES =
                "DROP TABLE IF EXISTS " + FeedContract.UserNote.TABLE_NAME;

        public FeedDatabase(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("DATABASE", "Creating database...");
            db.execSQL(SQL_CREATE_USERS);
            db.execSQL(SQL_CREATE_GEOS);
            db.execSQL(SQL_CREATE_LABELS);
            db.execSQL(SQL_CREATE_NOTES);
            db.execSQL(SQL_CREATE_ITEMS);
            db.execSQL(SQL_CREATE_USERS_NOTES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_USERS_NOTES);
            db.execSQL(SQL_DELETE_ITEMS);
            db.execSQL(SQL_DELETE_NOTES);
            db.execSQL(SQL_DELETE_LABELS);
            db.execSQL(SQL_DELETE_GEOS);
            db.execSQL(SQL_DELETE_USERS);

            onCreate(db);
        }
    }
}
