package com.notes.map.pinit.common;

/**
 * Created by Antonio on 26.1.2016..
 */
/*
 * Copyright (C) 2014 skyfish.jy@gmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

import android.app.Activity;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

/**
 * Created by skyfishjy on 10/31/14.
 */

public abstract class CursorRecyclerViewAdapter<VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> implements ContentObserverCallback {

    private Context mContext;

    private Cursor mCursor;

    private boolean mDataValid;

    private int mRowIdColumn;

    private ContentObserver mDataSetObserver;

    public CursorRecyclerViewAdapter(Context context, Cursor cursor) {
        mContext = context;
        mCursor = cursor;
        mDataValid = cursor != null;
        mRowIdColumn = mDataValid ? mCursor.getColumnIndex("_id") : -1;
        mDataSetObserver = new NotifyingContentObserver(this);
        if (mCursor != null) {
            mCursor.registerContentObserver(mDataSetObserver);
        }
    }

    public Cursor getCursor() {
        return mCursor;
    }

    @Override
    public int getItemCount() {
        if (mDataValid && mCursor != null) {
            return mCursor.getCount();
        }
        return 0;
    }

    @Override
    public long getItemId(int position) {
        if (mDataValid && mCursor != null && mCursor.moveToPosition(position)) {
            return mCursor.getLong(mRowIdColumn);
        }
        return 0;
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(true);
    }

    public abstract void onBindViewHolder(VH viewHolder, Cursor cursor);

    @Override
    public void onBindViewHolder(VH viewHolder, int position) {
        if (!mDataValid) {
            throw new IllegalStateException("this should only be called when the cursor is valid");
        }
        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("couldn't move cursor to position " + position);
        }
        onBindViewHolder(viewHolder, mCursor);
    }

    private class NotifyingContentObserver extends ContentObserver {

        private ContentObserverCallback mContentObserverCallback;

        public NotifyingContentObserver(ContentObserverCallback handler) {
            super(null);
            this.mContentObserverCallback = handler;
        }

        @Override
        public void onChange(boolean selfChange) {
            //todo this seems fishy...
            this.onChange(selfChange, null);
        }


        @Override
        public void onChange(boolean selfChange, Uri uri) {
            // this is NOT UI thread, this is a BACKGROUND thread
            Log.i(getClass().toString(), "Received onChange");
            mContentObserverCallback.update();
        }
    }

    public boolean isDataValid() {
        Log.d("CURSOR", "Cursor count: " + mCursor.getCount());
        return mCursor.getCount() != 0 && mDataValid;
    }

    /**
     * This is a ContentObserver callback that will be called when the data needs to be
     * requeried.
     *
     * TODO find how it's done now that requery is deprecated
     */
    public void update(){
        mCursor.requery();
        ((Activity) mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    /**
     * Use this to register an URI for the ContentObserver this class uses.
     *
     * Remember to ALWAYS call it in onResume() to avoid memory leak.
     *
     * @param context
     * @param uri
     */
    public void registerContentObserver(Context context, Uri uri){
        if (mDataSetObserver == null) {
            mDataSetObserver = new NotifyingContentObserver(this);
        }
        // register ContentObserver in onResume
        context.getContentResolver().
                registerContentObserver(
                        // observable uri
                        uri,
                        true,
                        mDataSetObserver);
    }

    /**
     * Use this to register an URI for the ContentObserver this class uses.
     *
     * Remember to ALWAYS call it in onPause() to avoid memory leaks.
     *
     * @param context
     */
    public void unregisterContentObserver(Context context){
        context.getContentResolver().unregisterContentObserver(mDataSetObserver);
    }
}