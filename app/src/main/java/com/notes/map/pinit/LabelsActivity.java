package com.notes.map.pinit;

import android.content.Context;
import android.content.DialogInterface;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.DisplayMetrics;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.services.ModelRepository;
import com.notes.map.pinit.services.SessionManager;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;

/**
 * Created by iamkr on 6.2.2016..
 */
public class LabelsActivity extends AppCompatActivity implements RecyclerClickListener.OnItemClickListener, View.OnClickListener  { //todo napravi s recycler view

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Toolbar mToolbar;
    private Cursor c;
    private RecyclerClickListener mRecyclerClickListener;

    @InjectView(R.id.button_add_label) ImageButton mAddButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labels);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_labels);
        setSupportActionBar(mToolbar);
        //getSupportActionBar().setDisplayShowHomeEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Labels");

        ButterKnife.inject(this);
        mAddButton.setOnClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_labels);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        ModelRepository repository = ModelRepository.getInstance(this.getApplicationContext());
        c = repository.findLabels();
        mAdapter = new LabelRecyclerViewAdapter(this, c);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerClickListener = new RecyclerClickListener(this, this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mRecyclerView.addOnItemTouchListener(mRecyclerClickListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mRecyclerView.removeOnItemTouchListener(mRecyclerClickListener);
    }

    @Override
    public void onItemClick(View childView, int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Edit label");
        c.moveToPosition(position);

        final EditText input = new EditText(this);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        final Label mLabel = Label.fromCursor(c);
        input.setText(mLabel.name);
        builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (!input.getText().toString().equals("")) {
                    mLabel.name = input.getText().toString();
                    try {
                        mLabel.commit(mLabel.save(getApplicationContext()));
                    } catch (RemoteException | OperationApplicationException e) {
                        Toast.makeText(getBaseContext(), "Failed to save Label!", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            }
        });
        builder.setNegativeButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    mLabel.delete();
                    mLabel.commit(mLabel.save(getApplicationContext()));
                } catch (RemoteException | OperationApplicationException e) {
                    Toast.makeText(getBaseContext(), "Failed to delete Label!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        });

        builder.show();
    }

    @Override
    public void onItemLongPress(View childView, int position) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.button_add_label:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Create label");

                final EditText input = new EditText(this);
                input.setInputType(InputType.TYPE_CLASS_TEXT);
                builder.setView(input);

                builder.setPositiveButton("Save", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (!input.getText().toString().equals("")) {
                            Label mLabel = new Label(input.getText().toString(), SessionManager.getInstance(getApplicationContext()
                            ).getSessionApiKey());
                            try {
                                mLabel.commit(mLabel.save(getApplicationContext()));
                            } catch (RemoteException | OperationApplicationException e) {
                                Toast.makeText(getBaseContext(), "Failed to save Label!", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show();
                break;
        }
    }
}
