package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

import java.util.List;

/**
 * Created by Antonio on 23.1.2016..
 */
public class UserNote extends Model{

    @Expose
    public final String notes_uuid;
    @Expose
    public final String users_api_key;
    @Expose
    public final int can_edit;
    @Expose
    public final int can_delete;
    @Expose
    public final int owner;
    @Expose
    public long date_updated;
    @Expose
    public int deleted;

    /**
     *
     * Short constructor, use for creating new UserNotes.
     *
     * @param notes_uuid
     * @param user_api_key
     * @param can_edit
     * @param can_delete
     * @param owner
     */
    public UserNote(String notes_uuid, String user_api_key, int can_edit, int can_delete, int owner) {
        this.notes_uuid = notes_uuid;
        this.users_api_key = user_api_key;
        this.can_edit = can_edit;
        this.can_delete = can_delete;
        this.owner = owner;
        this.date_updated = System.currentTimeMillis();
        this.deleted = 0;
    }

    /**
     *
     * Full constructor, without local ID. Use when pulling results from API.
     *
     * @param notes_uuid
     * @param user_api_key
     * @param can_edit
     * @param can_delete
     * @param owner
     * @param date_updated
     * @param deleted
     */
    public UserNote(String notes_uuid, String user_api_key, int can_edit, int can_delete, int owner, long date_updated, int deleted) {
        this(notes_uuid, user_api_key, can_edit, can_delete, owner);
        this.date_updated = date_updated;
        this.deleted = deleted;
    }

    /**
     * Full constructor, with local ID. Use when pulling results from local database.
     * @param _id
     * @param notes_uuid
     * @param user_api_key
     * @param can_edit
     * @param can_delete
     * @param owner
     * @param date_updated
     * @param deleted
     */
    public UserNote(long _id, String notes_uuid, String user_api_key, int can_edit, int can_delete, int owner, long date_updated, int deleted) {
        this(notes_uuid, user_api_key, can_edit, can_delete, owner, date_updated, deleted);
        this._id = _id;
    }

    @Override
    public List<ContentProviderOperation> save(Context context) {
        super.save(context);
        this.date_updated = System.currentTimeMillis();
        ContentValues values = new ContentValues();
        values.put(FeedContract.UserNote.COLUMN_NAME_CAN_EDIT, this.can_edit);
        values.put(FeedContract.UserNote.COLUMN_NAME_CAN_DELETE, this.can_delete);
        values.put(FeedContract.UserNote.COLUMN_NAME_OWNER, this.owner);
        values.put(FeedContract.UserNote.COLUMN_NAME_UPDATED, this.date_updated);
        values.put(FeedContract.UserNote.COLUMN_NAME_DELETED, this.deleted);
        if (this._id != 0){
            //update
            if (this.synced != FeedContract.SyncableItem.SYNC_STATUS_CREATED)
                values.put(FeedContract.UserNote.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_UPDATED);
            if (deleted == FeedContract.UserNote.DELETED)
                this.synced = FeedContract.SyncableItem.SYNC_STATUS_DELETED;
            String[] selection = {users_api_key, notes_uuid};
            mContentResolver.update(FeedContract.UserNote.CONTENT_URI, values, FeedContract.UserNote.COLUMN_NAME_API_KEY + "=? AND " + FeedContract.UserNote.COLUMN_NAME_NOTES_UUID + "=?", selection);
        } else {
            //insert new
            values.put(FeedContract.UserNote.COLUMN_NAME_API_KEY, this.users_api_key);
            values.put(FeedContract.UserNote.COLUMN_NAME_NOTES_UUID, this.notes_uuid);
            values.put(FeedContract.UserNote.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_CREATED);
            Uri uri = mContentResolver.insert(FeedContract.UserNote.CONTENT_URI, values);
            this._id = ContentUris.parseId(uri);
        }
        return batch;
    }

    @Override
    public void delete() {
        this.deleted = 1;
    }
}
