package com.notes.map.pinit;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RecyclerViewHolder extends RecyclerView.ViewHolder{

    TextView tv1, tv2, tvTodo1, tvTodo2, tvTodo3, tvMoreTodos, tvGeo1, tvGeo2, tvGeo3;
    LinearLayout todoLayout1, todoLayout2, todoLayout3;
    CheckBox checkBox1, checkBox2, checkBox3;
    LinearLayout mNoteCard, mTodoCard, mGeoNoteCard;

    public RecyclerViewHolder(View itemView) {
        super(itemView);
    }
}