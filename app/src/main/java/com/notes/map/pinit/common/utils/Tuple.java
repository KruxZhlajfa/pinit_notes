package com.notes.map.pinit.common.utils;

import java.util.Arrays;

/**
 * Created by Antonio on 26.1.2016..
 *
 * Source for this code: http://stackoverflow.com/questions/19805077/java-using-string-tuples-as-key-for-hashmap
 */
public class Tuple<T> {
    private final T[] contents;

    public Tuple (T[] contents) {
        if (contents.length != 2)
            throw new IllegalArgumentException();
        this.contents = contents;
    }

    public T[] getContents () {
        //todo does this need a clone? Seems heavy on performance
        return this.contents.clone();
    }

    @Override
    public int hashCode () {
        return Arrays.deepHashCode(this.contents);
    }

    @Override
    public boolean equals (Object other) {
        return Arrays.deepEquals(this.contents, ((Tuple<T>) other).getContents());
    }

    @Override
    public String toString () {
        return Arrays.deepToString(this.contents);
    }
}