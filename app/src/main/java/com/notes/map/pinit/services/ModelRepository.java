package com.notes.map.pinit.services;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.models.GeoNote;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.basicsyncadapter.models.Note;
import com.notes.map.pinit.basicsyncadapter.models.TodoNote;
import com.notes.map.pinit.basicsyncadapter.models.User;
import com.notes.map.pinit.basicsyncadapter.models.UserNote;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;
import com.notes.map.pinit.basicsyncadapter.sync.GeosSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.ItemsSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.LabelsSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.NotesSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.UserNotesSyncManager;
import com.notes.map.pinit.basicsyncadapter.sync.UsersSyncManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Antonio on 27.1.2016..
 *
 * A service class for finding different model data.
 *
 */

public class ModelRepository {

    private static ModelRepository instance;
    private Context mContext;
    private ContentResolver mContentResolver;

    private static final Map<String, Uri> classToUriMap;
    static {
        Map<String, Uri> aMap = new HashMap<>();
        aMap.put(Geo.class.getName(), FeedContract.Geo.CONTENT_URI);
        aMap.put(Item.class.getName(), FeedContract.Item.CONTENT_URI);
        aMap.put(Label.class.getName(), FeedContract.Label.CONTENT_URI);
        aMap.put(Note.class.getName(), FeedContract.Note.CONTENT_URI);
        aMap.put(TodoNote.class.getName(), FeedContract.Note.CONTENT_URI);
        aMap.put(GeoNote.class.getName(), FeedContract.Note.CONTENT_URI);
        aMap.put(User.class.getName(), FeedContract.User.CONTENT_URI);
        aMap.put(UserNote.class.getName(), FeedContract.UserNote.CONTENT_URI);
        classToUriMap = Collections.unmodifiableMap(aMap);
    }

    private static final Map<String, String[]> classToProjectionMap;
    static {
        Map<String, String[]> aMap = new HashMap<>();
        aMap.put(Geo.class.getName(), GeosSyncManager.PROJECTION);
        aMap.put(Item.class.getName(), ItemsSyncManager.PROJECTION);
        aMap.put(Label.class.getName(), LabelsSyncManager.PROJECTION);
        aMap.put(Note.class.getName(), NotesSyncManager.PROJECTION);
        aMap.put(TodoNote.class.getName(), NotesSyncManager.PROJECTION);
        aMap.put(GeoNote.class.getName(), NotesSyncManager.PROJECTION);
        aMap.put(User.class.getName(), UsersSyncManager.PROJECTION);
        aMap.put(UserNote.class.getName(), UserNotesSyncManager.PROJECTION);
        classToProjectionMap = Collections.unmodifiableMap(aMap);
    }

    private ModelRepository(Context context) {
        this.mContext = context;
    }

    /**
     * Make sure to pass this Singleton repository an instance of Context that is fetched using
     * getApplicationContext() to avoid significant memory leaks
     *
     * @param context
     * @return
     */
    public static ModelRepository getInstance(Context context){
        if (instance == null){
            instance = new ModelRepository(context);
        }
        return instance;
    }

    public Object findModelById(String className, String column_name, String id){
        if (mContentResolver == null)
            mContentResolver = mContext.getContentResolver();
        String[] args = {id};
        try {
            Class clazz  = Class.forName(className);
            Uri uri = classToUriMap.get(className);
            String[] projection =  classToProjectionMap.get(className);
            Cursor c = mContentResolver.query(uri, projection, column_name + "=?", args, null);
            if (c.getCount() == 1) {
                Method method = clazz.getMethod("fromInstance", Cursor.class);
                Object object = method.invoke(c);
                return object;
            }
        // todo handle errors
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param className
     * @return
     */
    public Cursor findModels(String className){
        if (mContentResolver == null)
            mContentResolver = mContext.getContentResolver();
        Cursor c = mContentResolver.query(classToUriMap.get(className), classToProjectionMap.get(className), null, null, null);
        return c;
    }

    /**
     *
     * @param className
     * @param selection
     * @param args
     * @param order
     * @return
     */
    public Cursor findModels(String className, String selection, String[] args, String order){
        if (mContentResolver == null)
            mContentResolver = mContext.getContentResolver();
        return mContentResolver.query(classToUriMap.get(className), classToProjectionMap.get(className), selection, args, order);
    }

    public Cursor findNotes(){
        String[] args = {""+FeedContract.Note.STATUS_NORMAL, ""+FeedContract.Note.STATUS_DONE};
        return findModels(Note.class.getName(), FeedContract.Note.COLUMN_NAME_STATUS + "=? OR "
                        + FeedContract.Note.COLUMN_NAME_STATUS + "=? AND "
                        + FeedContract.Note.COLUMN_NAME_TYPE + " != " + FeedContract.Note.TYPE_GEO, args,
                FeedContract.Note.COLUMN_NAME_ID + " DESC");
    }

    public Cursor findLabels(){
        return findModels(Label.class.getName(), FeedContract.Label.COLUMN_NAME_DELETED + "=0", null, FeedContract.Note.COLUMN_NAME_ID);
    }

    public Cursor findGeoNotes(){
        String[] args = {""+FeedContract.Note.STATUS_NORMAL, ""+FeedContract.Note.STATUS_DONE};
        /*return findModels(GeoNote.class.getName(), FeedContract.Note.COLUMN_NAME_STATUS + "=? OR "
                + FeedContract.Note.COLUMN_NAME_STATUS + "=? AND " + FeedContract.Note.COLUMN_NAME_TYPE + "=?", args, FeedContract.Note.COLUMN_NAME_ID);*/
        return findModels(GeoNote.class.getName(), "(" + FeedContract.Note.COLUMN_NAME_STATUS + "=? OR "
        + FeedContract.Note.COLUMN_NAME_STATUS + "=? ) AND " + FeedContract.Note.COLUMN_NAME_TYPE + "=2",
                args, FeedContract.Note.COLUMN_NAME_ID);
    }

}
