package com.notes.map.pinit.basicsyncadapter.models;

import android.database.Cursor;

import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

/**
 * Created by Antonio on 27.1.2016..
 */
public class NormalNote extends Note {

    public NormalNote(long id, String uuid, String title, long date_created, long date_updated, long date_alarm, String text, int priority, String color, int status, String labels_id, String geos_id) {
        super(id, uuid, title, date_created, date_updated, date_alarm, text, priority, color, status, labels_id, geos_id, FeedContract.Note.TYPE_NORMAL);
    }

    public static NormalNote fromCursor(Cursor cursor){
        NormalNote note = new NormalNote(
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UUID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TITLE)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_CREATED)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_UPDATED)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_ALARM)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_TEXT)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_PRIORITY)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_COLOR)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_STATUS)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_LABELS_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Note.COLUMN_NAME_GEOS_ID))
        );
        return note;
    }
}
