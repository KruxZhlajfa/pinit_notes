package com.notes.map.pinit.basicsyncadapter.collections;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.Label;
import com.notes.map.pinit.basicsyncadapter.models.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 */
public class LabelsCollection extends AbstractCollection {

    @Expose(serialize = false)
    public List<Label> conflicts = new ArrayList<>();

    @Expose
    public List<Label> labels = new ArrayList<>();

    public void add(Label label){
        this.labels.add(label);
    }

    public int size(){
        if (this.labels == null)
            return 0;
        else
            return this.labels.size();
    }

    @Override
    public Label get(String id) {
        for (Label label : labels){
            if (label.uuid.equals(id))
                return label;
        }
        return null;
    }
}
