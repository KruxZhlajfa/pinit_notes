/*
 * Copyright 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.notes.map.pinit.basicsyncadapter.provider;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Field and table name constants for
 * {@link com.notes.map.pinit.basicsyncadapter.provider.FeedProvider}.
 */

public class FeedContract {
    private FeedContract() {
    }

    /**
     * Content provider authority.
     */
    public static final String CONTENT_AUTHORITY = "com.notes.map.pinit.basicsyncadapter";
    /**
     * Base URI. (content://com.example.android.basicsyncadapter)
     */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    /**
     * Path component for different resource types.
     */
    private static final String PATH_NOTES = "notes";
    private static final String PATH_LABELS = "labels";
    private static final String PATH_GEOS = "geos";
    private static final String PATH_ITEMS = "items";
    private static final String PATH_USERS = "users";
    private static final String PATH_USERS_NOTES_USERS = "users";
    private static final String PATH_USERS_NOTES_NOTES = "notes";


    public static abstract class SyncableItem {

        /**
         * Deleted flag value
         */
        public static final int DELETED = 1;

        /**
         * Synced flag values
         */
        public static final int SYNC_STATUS_SYNCED = 0;
        public static final int SYNC_STATUS_CREATED = 1;
        public static final int SYNC_STATUS_UPDATED = 2;
        public static final int SYNC_STATUS_DELETED = 3;

        /**
         * Id
         */
        public static final String COLUMN_NAME_ID = "_id";

        /**
         * Synced flag
         */
        public static final String COLUMN_NAME_SYNCED = "synced";

    }

    /**
     * Columns supported by "notes" records.
     */
    public static class Note extends SyncableItem implements BaseColumns {

        public static final int TYPE_NORMAL = 0;
        public static final int TYPE_TODO = 1;
        public static final int TYPE_GEO = 2;

        public static final int STATUS_NORMAL = 0;
        public static final int STATUS_DONE = 1;
        public static final int STATUS_ARCHIVED = 2;
        public static final int STATUS_DELETED = 3;

        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pinit.notes";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.pinit.note";

        /**
         * Fully qualified URI for "entry" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_NOTES).build();

        /**
         * Table name where records are stored for "entry" resources.
         */
        public static final String TABLE_NAME = "notes";
        /**
         * Note title
         */
        public static final String COLUMN_NAME_TITLE = "title";
        /**
         * Date note was created
         */
        public static final String COLUMN_NAME_CREATED = "date_created";
        /**
         * Date note was updated
         */
        public static final String COLUMN_NAME_UPDATED= "date_updated";
        /**
         * Date of alarm notificiation
         */
        public static final String COLUMN_NAME_ALARM= "date_alarm";
        /**
         * Note text
         */
        public static final String COLUMN_NAME_TEXT= "text";
        /**
         * Note priority that user assigned to it
         */
        public static final String COLUMN_NAME_PRIORITY= "priority";
        /**
         * Note color that user assigned to it
         */
        public static final String COLUMN_NAME_COLOR= "color";
        /**
         * Note status (done, deleted, archived)
         */
        public static final String COLUMN_NAME_STATUS= "status";
        /**
         * Note UUID. Used for generating unique identifiers that can be pushed to the server to
         * uniquely identify each note no matter where it was created and how it is stored.
         */
        public static final String COLUMN_NAME_UUID = "uuid";

        /**
         * Labels UUID
         */
        public static final String COLUMN_NAME_LABELS_ID = "labels_id";
        /**
         * Geos UUID. Used to uniquely identify geo as foreign key.
         */
        public static final String COLUMN_NAME_GEOS_ID = "geos_id";

        /**
         * Geos UUID. Used to uniquely identify geo as foreign key.
         */
        public static final String COLUMN_NAME_TYPE = "type";
    }

    public static class Label extends SyncableItem implements BaseColumns {

        /**
         * Table name where records are stored for "entry" resources.
         */
        public static final String TABLE_NAME = "labels";

        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pinit.labels";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.pinit.label";

        /**
         * Fully qualified URI for "label" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_LABELS).build();


        public static final String COLUMN_NAME_UUID = "uuid";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_UPDATED = "date_updated";
        public static final String COLUMN_NAME_DELETED = "deleted";
        public static final String COLUMN_NAME_USERS_API_KEY = "users_api_key";

    }

    public static class Geo extends SyncableItem implements BaseColumns {

        /**
         * Table name where records are stored for "geo" resources.
         */
        public static final String TABLE_NAME = "geos";

        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pinit.geos";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.pinit.geo";

        /**
         * Fully qualified URI for "label" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_GEOS).build();

        public static final String COLUMN_NAME_UUID = "uuid";
        public static final String COLUMN_NAME_ADDRESS = "address";
        public static final String COLUMN_NAME_LONGITUDE = "longitude";
        public static final String COLUMN_NAME_LATITUDE = "latitude";
        public static final String COLUMN_NAME_RANGE = "range";
        public static final String COLUMN_NAME_UPDATED = "date_updated";
        public static final String COLUMN_NAME_DELETED = "deleted";


    }

    public static class Item extends SyncableItem implements BaseColumns {

        /**
         * Table name where records are stored for "geo" resources.
         */
        public static final String TABLE_NAME = "items";

        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pinit.items";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.pinit.item";

        /**
         * Fully qualified URI for "item" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_ITEMS).build();

        public static final String COLUMN_NAME_UUID = "uuid";
        public static final String COLUMN_NAME_TEXT = "text";
        public static final String COLUMN_NAME_CHECKED = "checked";
        public static final String COLUMN_NAME_UPDATED = "date_updated";
        public static final String COLUMN_NAME_NOTES_ID = "notes_id";
        public static final String COLUMN_NAME_DELETED = "deleted";

    }

    public static class User extends SyncableItem implements BaseColumns {

        /**
         * Table name where records are stored for "geo" resources.
         */
        public static final String TABLE_NAME = "users";

        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pinit.users";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.pinit.user";

        /**
         * Fully qualified URI for "user" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USERS).build();

        public static final String COLUMN_NAME_API_KEY = "user_email";
        public static final String COLUMN_NAME_EMAIL = "email";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SURNAME = "surname";
        public static final String COLUMN_NAME_PASSWORD = "password";
        public static final String COLUMN_NAME_PICTURE = "picture";
        public static final String COLUMN_NAME_DELETED = "deleted";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_UPDATED = "date_updated";

    }

    public static class UserNote extends SyncableItem implements BaseColumns {

        /**
         * Table name where records are stored for "geo" resources.
         */
        public static final String TABLE_NAME = "users_notes";

        /**
         * MIME type for lists of entries.
         */
        public static final String CONTENT_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.pinit.users_notes";
        /**
         * MIME type for individual entries.
         */
        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.pinit.user_note";

        /**
         * Fully qualified URI for "user_note" resources.
         */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_USERS_NOTES_USERS).appendPath(PATH_USERS_NOTES_NOTES).build();

        public static final String COLUMN_NAME_NOTES_UUID = "notes_uuid";
        public static final String COLUMN_NAME_API_KEY = "users_api_key";
        /*public static final String COLUMN_NAME_USERS_EMAIL = "users_email";*/
        public static final String COLUMN_NAME_CAN_EDIT = "can_edit";
        public static final String COLUMN_NAME_CAN_DELETE = "can_delete";
        public static final String COLUMN_NAME_OWNER = "owner";
        public static final String COLUMN_NAME_UPDATED = "date_updated";
        public static final String COLUMN_NAME_DELETED = "deleted";

    }

}