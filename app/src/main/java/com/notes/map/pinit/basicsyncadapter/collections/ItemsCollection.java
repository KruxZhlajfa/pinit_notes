package com.notes.map.pinit.basicsyncadapter.collections;


import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.models.Geo;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.Label;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 */
public class ItemsCollection extends AbstractCollection {

    @Expose(serialize = false)
    public List<Item> conflicts;

    @Expose
    public List<Item> items = new ArrayList<>();

    public void add(Item item){
        this.items.add(item);
    }

    public int size(){
        if (this.items == null)
            return 0;
        else
            return this.items.size();
    }

    @Override
    public Item get(String id) {
        for (Item item : items){
            if (item.uuid.equals(id));
                return item;
        }
        return null;
    }
}
