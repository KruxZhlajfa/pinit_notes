package com.notes.map.pinit.basicsyncadapter.models;

import android.content.ContentProviderOperation;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.provider.FeedContract;

import java.util.List;

/**
 * Created by Antonio on 22.1.2016..
 *
 * This class represents a single item in the JSON feed.
 *
 * <p>It includes all the data members
 *
 */
public class Item extends ModelWithUuid {

    @Expose
    public String text;
    @Expose
    public int checked;
    @Expose
    public long date_updated = System.currentTimeMillis();
    @Expose
    public String notes_id;
    @Expose
    public int deleted;

    public Item(){
        mUri = FeedContract.Item.CONTENT_URI;
    }

    /**
     * Full constructor, for access from local database
     * @param uuid
     * @param text
     * @param checked
     * @param date_updated
     * @param notes_id
     * @param deleted
     */
    public Item(long id, String uuid, String text, int checked, long date_updated, String notes_id, int deleted) {
        this(uuid, text, checked, notes_id);
        this.date_updated = date_updated;
        this._id = id;
        this.deleted = deleted;
    }

    /**
     * Full constructor
     * @param uuid
     * @param text
     * @param checked
     * @param date_updated
     * @param notes_id
     */
    public Item(String uuid, String text, int checked, long date_updated, String notes_id) {
        this(uuid, text, checked, notes_id);
        this.date_updated = date_updated;
    }

    /**
     * Used for creating new items
     * @param uuid
     * @param text
     * @param checked
     * @param notes_id
     */
    public Item(String uuid, String text, int checked, String notes_id){
        this();
        this.uuid = uuid;
        this.text = text;
        this.checked = checked;
        this.notes_id = notes_id;
    }

    public boolean isChecked(){
        return checked == 1;
    }

    public void setChecked(boolean b){
        checked = (b)? 1 : 0;
    }

    @Override
    public List<ContentProviderOperation> save(Context context) {
        super.save(context);
        this.date_updated = System.currentTimeMillis();
        if (this._id != 0){
            //update
            //let's check for the deleted status first
            int sync_status = FeedContract.SyncableItem.SYNC_STATUS_UPDATED;
            if (isDeleted())
                sync_status = FeedContract.SyncableItem.SYNC_STATUS_DELETED;
            Uri existingUri = mUri.buildUpon().appendPath(uuid).build();
            batch.add(ContentProviderOperation.newUpdate(existingUri)
                    .withValue(FeedContract.Item.COLUMN_NAME_TEXT, this.text)
                    .withValue(FeedContract.Item.COLUMN_NAME_UPDATED, this.date_updated)
                    .withValue(FeedContract.Item.COLUMN_NAME_CHECKED, this.checked)
                    .withValue(FeedContract.Item.COLUMN_NAME_NOTES_ID, this.notes_id)
                    .withValue(FeedContract.Item.COLUMN_NAME_UUID, this.uuid)
                    .withValue(FeedContract.Item.COLUMN_NAME_DELETED, this.deleted)
                    .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, sync_status)
                    .build());
        } else {
            //insert new
            batch.add(ContentProviderOperation.newInsert(mUri)
                    .withValue(FeedContract.Item.COLUMN_NAME_TEXT, this.text)
                    .withValue(FeedContract.Item.COLUMN_NAME_UPDATED, this.date_updated)
                    .withValue(FeedContract.Item.COLUMN_NAME_CHECKED, this.checked)
                    .withValue(FeedContract.Item.COLUMN_NAME_NOTES_ID, this.notes_id)
                    .withValue(FeedContract.Item.COLUMN_NAME_UUID, this.uuid)
                    .withValue(FeedContract.Item.COLUMN_NAME_DELETED, this.deleted)
                    .withValue(FeedContract.Item.COLUMN_NAME_SYNCED, FeedContract.SyncableItem.SYNC_STATUS_CREATED)
                    .build());
            /* this._id = ContentUris.parseId(uri); TODO see what about the IDs, how do we update the once the model is saved? */
        }
        return batch;
    }

    @Override
    public void delete() {
        this.deleted = 1;
    }

    public static Item fromCursor(Cursor cursor){
        return new Item(
                cursor.getLong(cursor.getColumnIndex(FeedContract.Item.COLUMN_NAME_ID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Item.COLUMN_NAME_UUID)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Item.COLUMN_NAME_TEXT)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Item.COLUMN_NAME_CHECKED)),
                cursor.getLong(cursor.getColumnIndex(FeedContract.Item.COLUMN_NAME_UPDATED)),
                cursor.getString(cursor.getColumnIndex(FeedContract.Item.COLUMN_NAME_NOTES_ID)),
                cursor.getInt(cursor.getColumnIndex(FeedContract.Item.COLUMN_NAME_DELETED)));
    }

    public boolean isDeleted(){
        return (deleted == 1)? true : false;
    }
}
