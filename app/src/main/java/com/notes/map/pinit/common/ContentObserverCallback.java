package com.notes.map.pinit.common;

/**
 * Created by Antonio on 27.1.2016..
 *
 * This is a callback that the ContentObserver uses to notify our classes that the underlying
 * dataset has changed.
 */
public interface ContentObserverCallback {
    void update();
}
