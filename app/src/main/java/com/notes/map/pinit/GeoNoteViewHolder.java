package com.notes.map.pinit;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by Nikolina on 12/02/2016.
 */
public class GeoNoteViewHolder extends RecyclerViewHolder{

    public GeoNoteViewHolder(View itemView) {
        super(itemView);
        tvGeo1 = (TextView) itemView.findViewById(R.id.card_geo_title);
        tvGeo2 = (TextView) itemView.findViewById(R.id.card_geo_desc);
        tvGeo3 = (TextView) itemView.findViewById(R.id.card_geo_address);
        mGeoNoteCard = (LinearLayout) itemView.findViewById(R.id.geo_card_layout);
    }
}
