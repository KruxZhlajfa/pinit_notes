package com.notes.map.pinit.basicsyncadapter.collections;

import com.google.gson.annotations.Expose;
import com.notes.map.pinit.basicsyncadapter.models.Item;
import com.notes.map.pinit.basicsyncadapter.models.UserNote;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Antonio on 23.1.2016..
 */
public class UsersNotesCollection extends AbstractCollection {

    @Expose(serialize = false)
    public List<UserNote> conflicts;

    @Expose
    public List<UserNote> users_notes = new ArrayList<>();

    public void add(UserNote userNote){
        this.users_notes.add(userNote);
    }

    public int size(){
        if (this.users_notes == null)
            return 0;
        else
            return this.users_notes.size();
    }

    @Override
    public UserNote get(String id) {
        return null;
    }

    public UserNote get(String userId, String noteId){
        for (UserNote userNote : users_notes){
            if (userNote.notes_uuid.equals(noteId) && userNote.users_api_key.equals(userId))
                return userNote;
        }
        return null;
    }
}
