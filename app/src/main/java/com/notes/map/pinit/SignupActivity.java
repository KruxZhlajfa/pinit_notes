package com.notes.map.pinit;

/**
 * Created by iamkr on 25.1.2016..
 */
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.notes.map.pinit.basicsyncadapter.PinitApiInterface;
import com.notes.map.pinit.basicsyncadapter.SyncAdapter;
import com.notes.map.pinit.basicsyncadapter.models.User;
import com.notes.map.pinit.services.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit2.Call;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class SignupActivity extends AppCompatActivity {
    private static final String TAG = "SignupActivity";
    private Response<User> response;
    private User mUser;

    @InjectView(R.id.input_name) EditText _nameText;
    @InjectView(R.id.input_surname) EditText _surnameText;
    @InjectView(R.id.input_email) EditText _emailText;
    @InjectView(R.id.input_password) EditText _passwordText;
    @InjectView(R.id.btn_signup) Button _signupButton;
    @InjectView(R.id.link_login) TextView _loginLink;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.inject(this);

        _signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signup();
            }
        });

        _loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish the registration screen and return to the Login activity
                finish();
            }
        });
    }


    public void signup() {
        Log.d(TAG, "Signup");

        if (!validate()) {
            return;
        }

        _signupButton.setEnabled(false);

        String name = _nameText.getText().toString();
        String surname = _surnameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        // TODO: Implement your own signup logic here.
        mUser = new User(email, name, surname, password);

        AsyncCaller sync = new AsyncCaller();
        sync.execute();
    }

    public void onSignupSuccess() {
        SessionManager sm = SessionManager.getInstance(getBaseContext());
        mUser.api_key = response.body().api_key;
        Log.d(getLocalClassName(), "API_KEY" + response.body().api_key);
        mUser.username = response.body().username;
        sm.createLoginSession(mUser);
        _signupButton.setEnabled(true);
        setResult(RESULT_OK, null);
        finish();
    }

    public void onSignupFailed() {
        try {
            String errors = response.errorBody().string();
            Log.d(this.getLocalClassName(), "Response:" + errors);
            JSONObject jsonObject = new JSONObject(errors);
            JSONArray jsonArray = jsonObject.getJSONArray("errors");
            for (int i = 0; i < jsonArray.length(); i++){
                Toast.makeText(getBaseContext(), jsonArray.getString(i), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Log.e(this.getLocalClassName(), e.toString());
            Toast.makeText(getBaseContext(), "Signup failed! Check your network connection!", Toast.LENGTH_SHORT).show();
        }
        _signupButton.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String name = _nameText.getText().toString();
        String surname = _surnameText.getText().toString();
        String email = _emailText.getText().toString();
        String password = _passwordText.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameText.setError("At least 3 characters");
            valid = false;
        } else {
            _nameText.setError(null);
        }

        if (surname.isEmpty() || surname.length() < 3) {
            _surnameText.setError("At least 3 characters");
            valid = false;
        } else {
            _surnameText.setError(null);
        }

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailText.setError("Enter a valid email address");
            valid = false;
        } else {
            _emailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 5) {
            _passwordText.setError("Password must be at least 8 characters");
            valid = false;
        } else {
            _passwordText.setError(null);
        }

        return valid;
    }

    private class AsyncCaller extends AsyncTask<Void, Void, Boolean>
    {

        final ProgressDialog progressDialog = new ProgressDialog(SignupActivity.this,
                R.style.AppTheme_Dark_Dialog);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setIndeterminate(true);
            progressDialog.setMessage("Creating Account...");
            progressDialog.show();
        }
        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Log.d("response", "email: " + mUser.email.toString());
                Log.d("response", "name: " + mUser.name.toString());
                Log.d("response", "surname: " + mUser.surname.toString());
                Log.d("response", "deleted: " + mUser.deleted);
                Log.d("response", "picture: " + mUser.picture);
                Log.d("response", "password: " + mUser.password.toString());

                Gson gson = new GsonBuilder().serializeNulls().create();
                //initialize client
                Retrofit client = new Retrofit.Builder()
                        .baseUrl(SyncAdapter.FEED_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .build();

                PinitApiInterface service = client.create(PinitApiInterface.class);

                Call<User> call = service.createUser(mUser);

                response = call.execute();

                Log.d("RESPONSE", "Response code:" + response.code());

                return response.isSuccess();
            } catch (Exception e) {
                Log.e("ASYNC EXCEPTION", e.getMessage());
                e.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            progressDialog.dismiss();
            if (aBoolean) {
                onSignupSuccess();
            } else {
                onSignupFailed();
            }
        }
    }

}
