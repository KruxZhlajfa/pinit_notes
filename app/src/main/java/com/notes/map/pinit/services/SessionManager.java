package com.notes.map.pinit.services;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.notes.map.pinit.LoginActivity;
import com.notes.map.pinit.basicsyncadapter.models.User;

/**
 * Created by iamkr on 3.2.2016..
 *
 * Session Manager Singleton is used for storing and accessing current users credentials that
 * are used for communicating with the API.
 *
 */

public class SessionManager {

    private static SessionManager instance;

    // Shared Preferences
    private SharedPreferences pref;

    // Editor for Shared preferences
    private Editor editor;

    // Context
    private Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "PinitPref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    public static final String KEY_API_KEY = "api_key";

    public static final String KEY_USERNAME = "username";

    public static final String KEY_NAME = "name";

    public static final String KEY_SURNAME = "surname";

    public static final String KEY_PICTURE = "picture";

    // Constructor
    private SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public static SessionManager getInstance(Context context){
        if (instance == null){
            instance = new SessionManager(context);
        }
        return instance;
    }


    /**
     * Puts the user data in SharedPrefs
     * @param user
     */
    private void putSessionUserData(User user){
        // Storing email in pref
        editor.putString(KEY_EMAIL, user.email);

        // Storing key in pref
        editor.putString(KEY_API_KEY, user.api_key);

        editor.putString(KEY_USERNAME, user.username);

        editor.putString(KEY_NAME, user.name);

        editor.putString(KEY_SURNAME, user.surname);

        editor.putString(KEY_PICTURE, user.picture);
    }

    /**
     * Create login session
     * */
    public void createLoginSession(User user){
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        putSessionUserData(user);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status.
     * If false it will redirect user to login page.
     * */
    public void checkLogin(){
        // Check login status
        if(!this.isLoggedIn()){
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }

    /**
     * Clear session details
     * */
    public void logoutUser(){
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Login Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     * **/
    // Get Login State
    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     * Returns the currently logged in user data, in a User object, for easier handling
     *
     * @return
     */
    public User getSessionUserData(){
        User user = new User(pref.getString(KEY_EMAIL, ""),
                pref.getString(KEY_NAME, ""),
                pref.getString(KEY_SURNAME, ""),
                0,
                pref.getString(KEY_USERNAME, ""));
        //todo see if storing a picture in SharedPreferences is a good idea
        user.picture = pref.getString(KEY_PICTURE, "");
        user.api_key = pref.getString(KEY_API_KEY, "");
        return user;
    }

    /**
     * Used for updating the UserData in the session.
     * Call after successful push of new data to server.
     *
     * @param user
     */
    public void updateSessionUserData(User user){

        putSessionUserData(user);

        editor.commit();
    }

    /**
     * Returns the API key that is used for communicating with the API
     * @return
     */
    public String getSessionApiKey(){
        return pref.getString(KEY_API_KEY, "");
    }

    //todo see if it makes more sense to move this into the ModelRepository layer, or even SyncAdapter

    public void storeSyncTimestamp(String key, long timestamp){
        editor.putLong(key, timestamp);
        editor.commit();
    }

    public long getSyncTimestamp(String key){
        return pref.getLong(key, 0);
    }
}
